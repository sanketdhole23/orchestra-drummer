#include <Servo.h>
// Song Name :Badtmeez Dil , Movie Name: Yeh Jawaani hai Dewaani , 
int i, j,k;

int delay_BadtmeezDil = 143.25;
int delay_ChammakChallo = 107;
int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int NeckPin = 10;
int HihatPin = 11;
int SnarePin = 12;
int Snare_up = 95;
int Floor_up = 70;
int Tom_H_up = 80;
int Tom_M_up = 85;
int Neck_up = 90;
int Hihat_up = 90;
int Kick_up = 90;
int Snare_down = 105;
int Floor_down = 60;
int Tom_H_down = 70;
int Tom_M_down = 95;
int Neck_down = 140;
int Hihat_down = 80;
int Kick_down = 105;
int Current_angle = 90;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Neck;    //C
Servo Hihat;     //T
Servo Kick;     //K
                      // C+K = L   T+K = N   T+S = P  M+T+S+K = Q   M+K = R   S+K = U
char Beat_BadtmeezDil_A[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','0','0'};
char Beat_BadtmeezDil_B[20] = {'K','0','0','S','K','0','S','0','K','0','0','S','K','0','S','0'};
char Beat_BadtmeezDil_C[20] = {'K','0','0','S','K','0','S','0','K','0','0','S','0','0','0','0'};
char Beat_BadtmeezDil_D[20] = {'K','0','S','S','0','K','0','K','S','0','0','0','0','0','0','0'};
char Beat_BadtmeezDil_E[20] = {'K','K','0','S','K','0','S','0','S','S','K','K','S','0','0','0'};
char Beat_BadtmeezDil_F[20] = {'K','0','0','0','S','0','0','0','0','0','K','0','S','0','0','0'};
char Beat_BadtmeezDil_G[20] = {'K','0','0','0','S','0','0','S','0','K','K','0','S','0','0','0'};
char Beat_BadtmeezDil_H[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','S','0'};
char Beat_BadtmeezDil_I[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','S','S'};
char Beat_BadtmeezDil_J[20] = {'K','0','0','0','K','0','0','0','K','0','0','S','S','0','0','0'};

char note_Chammak_Challo_A[20] = {'S','0','0','S','S','0','S','0','H','0','H','0','M','0','0','0'};
char note_Chammak_Challo_B[20] = {'L','0','0','0','S','0','S','0','0','0','K','0','S','0','0','0'};
char note_Chammak_Challo_C[20] = {'L','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};
void setup() {
   // put your setup code here, to run once:
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Neck.attach(NeckPin);
  Hihat.attach(HihatPin);
  
  Serial.begin(9600);
  //while (!Serial);
 // Serial.println("Drummer_ready");
 }

void Neckmovement(){
  if(Current_angle == Neck_up){
  Neck.write(Neck_down);
  Current_angle = Neck_down;
  }
else if (Current_angle == Neck_down){
  Neck.write(Neck_up);
  Current_angle = Neck_up;
  }
}

//==============================================Badtmeez Dil

void noteBadtmeezDil_B(){
      if(Beat_BadtmeezDil_B[i] == 'K'){
        Kick.write(Kick_down);
        //Serial.println(i);
        }
      else if(Beat_BadtmeezDil_B[i] == 'S'){
         Snare.write(Snare_down);
         //Serial.println(i);
          }
}

void noteBadtmeezDil_D(){
        if(Beat_BadtmeezDil_D[i] == 'K'){
           Kick.write(Kick_down);
          // Serial.println(i);
          }
        else if(Beat_BadtmeezDil_D[i] == 'S'){
           Snare.write(Snare_down);
           //Serial.println(i);
          }
}

void noteBadtmeezDil_E(){
        if(Beat_BadtmeezDil_E[i] == 'K'){
           Kick.write(Kick_down);
          // Serial.println(i);
            }
        
         else if(Beat_BadtmeezDil_E[i] == 'S'){
           Snare.write(Snare_down);
           //Serial.println(i);
            }
}
void BeatBadtmeezDil_1(){
for(j = 2; j<=5; j++){ 
    for(i=0; i<=15; i++) {
        if(j == 5){
            if(Beat_BadtmeezDil_J[i] == 'K'){
                   Kick.write(Kick_down);
                  // Serial.println(i);
                   }
            else if(Beat_BadtmeezDil_J[i] == 'S'){
                   Snare.write(Snare_down);
                   //Serial.println(i);
                   }    
                }
        else{
            if(Beat_BadtmeezDil_A[i] == 'K'){
            Kick.write(Kick_down);
            //Serial.println(i);
            }
        }
        delay(delay_BadtmeezDil);
          Kick.write(Kick_up);
          Snare.write(Snare_up);
      }
      Neckmovement();
  }
}

//=====================================================
void BeatBadtmeezDil_2(){
  for(j=6; j<= 39; j++){
    for (i=0; i<=15; i++){
        if(j == 13 || j == 21 || j == 31){
            if(Beat_BadtmeezDil_C[i] == 'K'){
              Kick.write(Kick_down);
              //Serial.println(i);
              }
        
            else if(Beat_BadtmeezDil_C[i] == 'S'){
              Snare.write(Snare_down);
              //Serial.println(i);
            }
        }
        else if(j == 39){
              noteBadtmeezDil_E();
          }
        else {
          noteBadtmeezDil_B();
        }
      delay(delay_BadtmeezDil);
      Kick.write(Kick_up);
      Snare.write(Snare_up);
    }
  }
}
//=======================================================
void BeatBadtmeezDil_4(){
  for(j=40; j<=47; j++){
        for(i=0; i<=15; i++){
            if(j == 43){
                if(Beat_BadtmeezDil_G[i] == 'K'){
                    Kick.write(Kick_down);
                   // Serial.println(i);     
                    }
                else if(Beat_BadtmeezDil_G[i] == 'S'){
                   Snare.write(Snare_down);
                   //Serial.println(i);
                    }
              }
            else{
              if(Beat_BadtmeezDil_F[i] == 'K'){
                Kick.write(Kick_down);
                //Serial.println(i);
                }
              else if(Beat_BadtmeezDil_F[i] == 'S'){
                 Snare.write(Snare_down);
                 //Serial.println(i);
                  }
            }
            delay(delay_BadtmeezDil);
            Kick.write(Kick_up);
            Snare.write(Snare_up);
      }
  }
}
//=========================================================
void BeatBadtmeezDil_5(){
  for(j=48; j<=55; j++){
 
      for(i=0; i<=15; i++){
          if(j == 49 || j == 53){
                if(Beat_BadtmeezDil_H[i] == 'K'){
                    Kick.write(Kick_down);
                   // Serial.println(i);
                    }
                else if(Beat_BadtmeezDil_H[i] == 'S'){
                    Snare.write(Snare_down);
                    //Serial.println(i);
                    }    
            }
           else if(j == 51){
                if(Beat_BadtmeezDil_I[i] == 'K'){
                   Kick.write(Kick_down);
                 //  Serial.println(i);
                   }
                else if(Beat_BadtmeezDil_I[i] == 'S'){
                   Snare.write(Snare_down);
                   //Serial.println(i);
                   }    
               }
           else {
                  if(Beat_BadtmeezDil_A[i] == 'K'){
                  Kick.write(Kick_down);
                 // Serial.println(i);
                  }
            }
      delay(delay_BadtmeezDil);
      Kick.write(Kick_up);
      Snare.write(Snare_up); 
      }
  }
}
//============================================================
void BeatBadtmeezDil_6(){
  for(j = 56; j<=79 ; j++){
      for(i=0; i<=15; i++) {
      //Serial.println(i);
          if(j == 61 || j == 71){
              noteBadtmeezDil_D();
          } 
          else if(j == 79){
              noteBadtmeezDil_E();
          }
          else{
              noteBadtmeezDil_B();
          }
        delay(delay_BadtmeezDil);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
      }
  }
}
//=========================================================
void BeatBadtmeezDil_9(){
  for(i=0; i<=15; i++) {
      //Serial.println(i);
        if(Beat_BadtmeezDil_A[i] == 'K'){
         Kick.write(Kick_down);
         //Serial.println(i);
        }
        delay(delay_BadtmeezDil);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
     }
  BeatBadtmeezDil_5();
}
//=========================================================================
void BeatBadtmeezDil_10(){
  for(j = 89; j<=102 ; j++){
      for(i=0; i<=15; i++) {
      //Serial.println(i);
          if(j == 94){
              noteBadtmeezDil_D();
          }  
          else if(j == 102){
              noteBadtmeezDil_E();
          }
          else{
              noteBadtmeezDil_B();
          }
        delay(delay_BadtmeezDil);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
      }
  }
}
void BadtmeezDil_Track(){
  BeatBadtmeezDil_1();
  BeatBadtmeezDil_2();
  BeatBadtmeezDil_4();
  BeatBadtmeezDil_5();
  BeatBadtmeezDil_6();
  BeatBadtmeezDil_9();
  BeatBadtmeezDil_10();
}
//===================================================Chammak Challo

void Chammak_Challo_Fill() {

  for (i = 0; i <=15; i++) {
      if(note_Chammak_Challo_A[i] == 'H'){
        Tom_H.write(Tom_H_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_A[i] == 'M'){
        Tom_M.write(Tom_M_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_A[i] == 'F'){
        Floor_Tom.write(Floor_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_A[i] == 'S'){
        Snare.write(Snare_down);
        //Serial.println(i);
      }
      delay(delay_ChammakChallo);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  } 
}

void Chammak_Challo_Groove1() {
for(i = 0; i <= 15 ;i++){ 
      if(note_Chammak_Challo_B[i] == 'H'){
        Tom_H.write(Tom_H_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'M'){
        Tom_M.write(Tom_M_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'F'){
        Floor_Tom.write(Floor_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'S'){
        Snare.write(Snare_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'K'){
        Kick.write(Kick_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'L'){
        Kick.write(Kick_down);
        //Serial.println(i);
      }
      delay(delay_ChammakChallo);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  } 
}

void Chammak_Challo_Groove3(){
  for(j=0; j<3; j++){  
      Chammak_Challo_Groove1();
  }
}

void Chammak_Challo_Groove4(){
  for(j=0; j<4; j++){  
      Chammak_Challo_Groove1();
  }
  
}
void Chammak_Challo_Groove7(){
      Chammak_Challo_Groove3();
      Chammak_Challo_Groove4();
}

void Chammak_Challo_Blank(){
  for(i = 0; i<=15 ; i++){
      if(note_Chammak_Challo_C[i] == 'L'){
        Kick.write(Kick_down);
       // Serial.println(i);
      }
    delay(delay_ChammakChallo);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  }
}

void Chammak_Challo_Fill_Groove7(){
  Chammak_Challo_Fill();
  Chammak_Challo_Groove7();
}

void Chammak_Challo_Fill_Groove3(){
  Chammak_Challo_Fill();
  Chammak_Challo_Groove3();
}
void Chammak_Challo_Blank_Groove3(){
  Chammak_Challo_Blank();
  Chammak_Challo_Groove3();
}

void Chammak_Challo_Blank_Groove7(){
  Chammak_Challo_Blank();
  Chammak_Challo_Groove7();
}

void Chammak_Challo_Track(){
        delay(1000);
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Blank_Groove3();
        Chammak_Challo_Blank_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Blank_Groove3();
        Chammak_Challo_Blank_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Blank_Groove7();
        Chammak_Challo_Groove4();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill();
}
//==============================================================
void loop()  {
 
  if (Serial.available())
  {
    int state = Serial.parseInt();   
    //char state = Serial.read();
    //Serial.println(state);
      
      if (state == 2)
      { 
        //Serial.print("Drumm");
        delay(1500);
        BadtmeezDil_Track();
      }
      else if(state == 3){
        //Serial.print("Drumm");
        Chammak_Challo_Track();    
      }
  }
}
