from __future__ import division
import time
import Adafruit_PCA9685
import pygame
pygame.mixer.init()
pwm = Adafruit_PCA9685.PCA9685()
servo_min = 150  # Min pulse length out of 4096
servo_max = 600  # Max pulse length out of 4096

# Helper function to make setting a servo pulse width simpler.
def set_servo_pulse(channel, pulse):
    pulse_length = 1000000    # 1,000,000 us per second
    pulse_length //= 60       # 60 Hz
    print('{0}us per period'.format(pulse_length))
    pulse_length //= 4096     # 12 bits of resolution
    print('{0}us per bit'.format(pulse_length))
    pulse *= 1000
    pulse //= pulse_length
    pwm.set_pwm(channel, 0, pulse)

# Set frequency to 60hz, good for servos.
class Myclass:
    pwm.set_pwm_freq(60)
    pygame.mixer.music.load("/home/pi/manoj/medifiles/national_anthem.mid")
    pygame.mixer.music.play()
class Myclass1:
    def beat1():
            for i in range(0,24):
            
                pwm.set_pwm(0, 0, 150)
                pwm.set_pwm(1, 0, 150)
                pwm.set_pwm(2, 0, 150)
                pwm.set_pwm(3, 0, 150)
                pwm.set_pwm(4, 0, 150)
                pwm.set_pwm(5, 0, 150)
                time.sleep(0.5)
                pwm.set_pwm(0, 0, 600)
                pwm.set_pwm(1, 0, 600)
                pwm.set_pwm(2, 0, 600)
                pwm.set_pwm(3, 0, 600)
                pwm.set_pwm(4, 0, 600)
                pwm.set_pwm(5, 0, 600)
                time.sleep(0.5)
                print("M")
    def beat2():
        for i in range(0,7):
                
                pwm.set_pwm(0, 0, 150)
                pwm.set_pwm(1, 0, 150)
                pwm.set_pwm(2, 0, 150)
                pwm.set_pwm(3, 0, 150)
                pwm.set_pwm(4, 0, 150)
                pwm.set_pwm(5, 0, 150)
                time.sleep(0.5)
                pwm.set_pwm(0, 0, 263)
                pwm.set_pwm(1, 0, 263)
                pwm.set_pwm(2, 0, 263)
                pwm.set_pwm(3, 0, 263)
                pwm.set_pwm(4, 0, 263)
                pwm.set_pwm(5, 0, 263)
                time.sleep(0.5)
                print("A")
    def beat3():
        for i in range(0,3):
            for i in range(0,2):
                    pwm.set_pwm(0, 0, 150)
                    pwm.set_pwm(1, 0, 150)
                    pwm.set_pwm(2, 0, 150)
                    pwm.set_pwm(3, 0, 150)
                    pwm.set_pwm(4, 0, 150)
                    pwm.set_pwm(5, 0, 150)
                    time.sleep(0.25)
                    pwm.set_pwm(0, 0, 263)
                    pwm.set_pwm(1, 0, 263)
                    pwm.set_pwm(2, 0, 263)
                    pwm.set_pwm(3, 0, 263)
                    pwm.set_pwm(4, 0, 263)
                    pwm.set_pwm(5, 0, 263)
                    time.sleep(0.25)
                    print("N")
            time.sleep(0.5)
    def beat4():
                pwm.set_pwm(0, 0, 150)
                pwm.set_pwm(1, 0, 150)
                pwm.set_pwm(2, 0, 150)
                pwm.set_pwm(3, 0, 150)
                pwm.set_pwm(4, 0, 150)
                pwm.set_pwm(5, 0, 150)
                time.sleep(0.5)
                pwm.set_pwm(0, 0, 263)
                pwm.set_pwm(1, 0, 263)
                pwm.set_pwm(2, 0, 263)
                pwm.set_pwm(3, 0, 263)
                pwm.set_pwm(4, 0, 263)
                pwm.set_pwm(5, 0, 263)
                time.sleep(0.5)
                print("o")

if __name__=='__main__':
    beat1()
    time.sleep(12)
    beat2()
    time.sleep(1)
    beat3()
    beat4()
    time.sleep(3)


    
         ## Hihat.stop()
         ## Bass.stop()
 #   GPIO.cleanup()
