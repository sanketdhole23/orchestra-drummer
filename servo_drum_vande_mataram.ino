#include <Servo.h> 

// Program for National Anthem
int i,j;
int servoPin1 = 10;
int servoPin2 = 11;
int servoPin3 = 12;
Servo Hihat;
Servo Kick;
Servo Snare;

void setup() {
   // put your setup code here, to run once:
  Hihat.attach(servoPin1);
  Kick.attach(servoPin2);
  Snare.attach(servoPin3);
}


void beat1(){

  Kick.write(0);
  Hihat.write(0);
  delay(250);
  Kick.write(45);
  Hihat.write(45);
  delay(250);
  
  Hihat.write(0);
  delay(250);
  Hihat.write(45);
  delay(250);
  
  Hihat.write(0);
  Snare.write(0);
  delay(250);
  Hihat.write(45);
  Snare.write(45);
  delay(250);
  
  Hihat.write(0);
  delay(250);
  Hihat.write(45);
  delay(250);
}

void beat2(){

  Kick.write(0);
  Hihat.write(0);
  delay(250);
  Kick.write(45);
  Hihat.write(45);
  delay(250);

  Kick.write(0);
  Hihat.write(0);
  delay(250);
  Kick.write(45);
  Hihat.write(45);
  delay(250);
  
  Hihat.write(0);
  Snare.write(0);
  delay(250);
  Hihat.write(45);
  Snare.write(45);
  delay(250);
  
  Hihat.write(0);
  delay(250);
  Hihat.write(45);
  delay(250);
  
}

void beat3() {
  
  Kick.write(0);
  Hihat.write(0);
  delay(250);
  Kick.write(45);
  Hihat.write(45);
  delay(250);

  Kick.write(0);
  Hihat.write(0);
  delay(250);
  Kick.write(45);
  Hihat.write(45);
  delay(250);
  
  Hihat.write(0);
  Snare.write(0);
  delay(250);
  Hihat.write(45);
  Snare.write(45);
  delay(250);
  
  Hihat.write(0);
  Snare.write(0);
  delay(250);
  Hihat.write(45);
  Snare.write(45);
  delay(250);
}


void loop()  {

for( i = 0; i<8 ; i++)  {
  
  beat1();
  beat2();
  beat1();
  beat3();
  }
}

