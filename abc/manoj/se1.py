import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)
GPIO.setup(15,GPIO.OUT)
pwm =GPIO.PWM(15,50)
pwm.start(5)
pwm.ChangeDutyCycle(10)
time.sleep(1)
pwm.ChangeDutyCycle(50)
time.sleep(1)
pwm.stop()
GPIO.cleanup()
