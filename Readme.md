#Drummer Robot Setup
###The Drummer orchestra setup will include 7 piece drumset along with Inmoov designed with four arms to play drumset. Each arm is designed and assigned to play individual drum.
###Left outer arm has been assigned to Floor tom, Left inner arm has been assigned to High tom, Right inner arm to Low tom and Right outer arm to Snare drum. The kick drum is controlled
###with individual motor.

#Arduino IDE installation
###Drummer arm motors are controlled using Arduino Mega. Initially, Download and install latest version of Arduino IDE on your system from official Arduino website. Complete the istalltion
###process and start the arduino IDE. Connect Arduino Mega to CPU via USB cable. Go to sketch option in the left upper corner and select the option include library>Manage libraries. Search 
###and install Servo library from the library window.

#Hardware Connection
###Write suitable algorithm in main window for movement of motors as per song. Make All connection between motors and power supply by connecting ground and supply pins. Connect all data
###pins fro motors to arduino as programmed. Please make sure the ground pin is connected commonly between power supply , motors and Arduino. After making sure all connection are in place,
###compile and upload the code to Arduino.

#Writing and uploading code to IDE
###Before uploading the code to Arduino make sure to select AVRISP mkll in Tools>Programmer>AVRISP mkll. Also make sure to select Tools>Boards>Arduino Mega. Select the specific port by selecting
###Tools>Port. After completing abpve procedure upload the code to arduino. The succesful uploading will show "Done compiling" on bottom left side of window. Remove the USB from CPU end and 
###connect it to Raspberry Pi USB port.

###The Raspberry pi acts as a server to send command to arduino for selection of song to be played on drumset. Rasberry pi recieves particular song number from main server and is forwarded to 
###Arduino Serial cable. On recieving song number from main server, Raspberry pi plays the song as well forward the song number to arduino. The Arduino and Raspberry pi in this way moves motor 
###and plays song in parallel and in syncrhonzation.
 



