import socketserver
import json

class MyTCPServer(socketserver.ThreadingTCPServer):
    allow_reuse_address = True

class MyTCPServerHandler(socketserver.BaseRequestHandler):
    def handle(self):
        try:
            data = json.loads(self.request.recv(1024).strip())
            # process the data, i.e. print it:
            print("%s"%data)
            # send some 'ok' back
            self.request.sendall(bytes(json.dumps({'return':'tempLog.dat'})))
        except Exception as e:
            print ("Exception wile receiving message:%s "%e)

#server = MyTCPServer(('192.168.1.175', 13373), MyTCPServerHandler)
server = MyTCPServer(('127.0.0.1', 13374), MyTCPServerHandler)
server.serve_forever()
