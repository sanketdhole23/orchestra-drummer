#include <Servo.h>
// Song Name :Dhoom Machalle , Movie Name: Dhoom , 
int i, j;
int delay_Dhoom = 142;
int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int NeckPin = 10;
int HihatPin = 11;
int SnarePin = 12;
int Snare_up = 95;
int Floor_up = 70;
int Tom_H_up = 80;
int Tom_M_up = 85;
int Neck_up = 90;
int Hihat_up = 90;
int Kick_up = 90;
int Snare_down = 105;
int Floor_down = 60;
int Tom_H_down = 70;
int Tom_M_down = 95;
int Neck_down = 140;
int Hihat_down = 80;
int Kick_down = 105;
int Current_angle = 90;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Neck;    //C
Servo Hihat;     //T
Servo Kick;     //K
                // C+K = L   T+K = N   T+S = P  M+T+S+K = Q   M+K = R   S+K = U
char note_Dhoom_A[20] = {'N','0','T','0','N','0','T','0','N','0','T','0','N','0','T','0'};
char note_Dhoom_B[20] = {'N','0','N','0','N','0','N','0','N','0','N','0','N','0','N','0'};
char note_Dhoom_C[20] = {'N','0','P','K','T','K','P','0','N','0','P','0','N','0','P','0'};
char note_Dhoom_D[20] = {'N','0','P','0','N','0','P','0','N','0','P','0','N','0','P','0'};

void setup()
{
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Neck.attach(NeckPin);
  Hihat.attach(HihatPin);

  pinMode(13, OUTPUT);
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Drummer_ready");
}

void Neckmovement(){
  if(Current_angle == Neck_up){
  Neck.write(Neck_down);
  Current_angle = Neck_down;
  }
else if (Current_angle == Neck_down){
  Neck.write(Neck_up);
  Current_angle = Neck_up;
  }
}

void DhoomMachalle_1() {
for(j = 0; j<=2 ; j++){
  for (i = 0; i <=15; i++) {
      if(note_Dhoom_A[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Dhoom_A[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_Dhoom);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  }
  Neckmovement(); 
  }
}

void DhoomMachalle_2() {
  for (i = 0; i <=15; i++) {
      if(note_Dhoom_B[i] == 'N'){
        Hihat.write(Hihat_down);
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(delay_Dhoom);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  }
  Neckmovement(); 
}

void DhoomMachalle_3() {
  for (i = 0; i <=15; i++) {
      if(note_Dhoom_C[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Dhoom_C[i] == 'P'){
        Hihat.write(Hihat_down);
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Dhoom_C[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      else if(note_Dhoom_C[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_Dhoom);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  }
  Neckmovement(); 
}

void DhoomMachalle_4() {
  for (i = 0; i <=15; i++) {
      if(note_Dhoom_D[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Dhoom_D[i] == 'P'){
        Hihat.write(Hihat_down);
        Snare.write(Snare_down);
        Serial.println(i);
      }
      delay(delay_Dhoom);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  }
  Neckmovement(); 
}

void DhoomMachalle_5(){
  for (j = 10; j <= 93; j++){
     if(j == 21 || j == 77){
      DhoomMachalle_4();
     }
     else if(j == 22 || j == 78){
      Serial.println("Silent loop");
      delay(2280);
     }
     else{
       DhoomMachalle_3();
     }
  }
}
void loop(){
  if (Serial.available())
  {
    int state = Serial.parseInt();
    Serial.println(state);
    Serial.println("Drummer_ready");
      if (state == 8)
      {
        delay(11000);
        DhoomMachalle_1();
        DhoomMachalle_2();
        DhoomMachalle_5();
        Serial.println("THE END");
      }
      }
}
