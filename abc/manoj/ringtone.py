import RPi.GPIO as GPIO                     ## Import GPIO Library.
import time
import pygame
import threading
from threading import Thread
pygame.mixer.init()
GPIO.setmode(GPIO.BOARD)
GPIO.setup(22,GPIO.OUT)
pwm=GPIO.PWM(22,50)
angle1=0
duty1= float(angle1)/10 + 2.5               ## Angle To Duty cycle  Conversion
angle2=180
duty2= float(angle2)/10 + 2.5
def audio():
     pygame.mixer.music.load("/home/pi/Downloads/20863.mp3")
     pygame.mixer.music.play()
     while pygame.mixer.music.get_busy() == True:
         continue
    
     ##  print("2")
     return audio()
     

def servo():
    
     pwm.start(2)
     ck=0
     while ck<=1:
          pwm.ChangeDutyCycle(duty1)
          time.sleep(1)
          pwm.ChangeDutyCycle(duty2)
          time.sleep(1)
          ck=ck+1
          
     time.sleep(1)
     ##  print("1")
     return servo()


if __name__ == '__main__':
   
   Thread(target = servo).start()
   Thread(target = audio).start()

    
GPIO.cleanup()




