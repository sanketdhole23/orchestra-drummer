#include <Servo.h>
// Song Name :Laila Me Laila, Movie Name: , 
int i, j, k;
int delay_LailaMeLaila = 107.79;
int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int NeckPin = 10;
int HihatPin = 11;
int SnarePin = 12;
int Snare_up = 95;
int Floor_up = 70;
int Tom_H_up = 80;
int Tom_M_up = 85;
int Neck_up = 90;
int Hihat_up = 90;
int Kick_up = 90;
int Snare_down = 105;
int Floor_down = 60;
int Tom_H_down = 70;
int Tom_M_down = 95;
int Neck_down = 140;
int Hihat_down = 80;
int Kick_down = 105;
int Current_angle = 90;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Neck;    //C
Servo Hihat;     //T
Servo Kick;     //K
                // C+K = L   T+K = N   T+S = P  M+T+S+K = Q   M+K = R   S+K = U  M+F+K = V  M+F = W   T+S+K = X
char Beat_LailaMeLaila_A[20] = {'V','0','W','0','V','0','0','0','V','W','0','0','V','W','0','0'};  //4
char Beat_LailaMeLaila_B[20] = {'V','0','W','0','V','0','W','0','V','0','W','0','V','0','W','0'};  //12                                                             
char Beat_LailaMeLaila_C[20] = {'K','0','T','S','N','0','P','0','N','0','T','S','N','0','P','0'};  //14
char Beat_LailaMeLaila_D[20] = {'N','0','T','S','N','0','P','0','N','0','T','S','N','0','P','0'};  //15& 20
char Beat_LailaMeLaila_E[20] = {'N','0','T','S','N','0','P','0','N','0','T','S','N','0','S','S'};  //21
char Beat_LailaMeLaila_F[20] = {'S','0','S','0','H','0','H','0','M','0','M','0','F','0','F','0'};  //45&70
char Beat_LailaMeLaila_G[20] = {'K','0','0','K','T','0','K','0','T','0','0','0','T','0','0','0'};  //62
char Beat_LailaMeLaila_H[20] = {'N','0','0','K','T','0','K','0','T','0','0','0','T','0','0','0'};  //63
char Beat_LailaMeLaila_I[20] = {'K','0','0','0','N','0','T','0','N','0','T','0','N','0','T','0'};  //71
char Beat_LailaMeLaila_J[20] = {'N','0','T','0','N','0','T','0','N','0','T','0','N','0','T','0'};  //72
char Beat_LailaMeLaila_K[20] = {'V','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};  //5
void setup() {
   // put your setup code here, to run once:
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Neck.attach(NeckPin);
  Hihat.attach(HihatPin);
  
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Drummer_ready");
 }

//===========================================================

void Neckmovement(){
  if(Current_angle == Neck_up){
  Neck.write(Neck_down);
  Current_angle = Neck_down;
  }
else if (Current_angle == Neck_down){
  Neck.write(Neck_up);
  Current_angle = Neck_up;
  }
}


void noteLailaMeLaila_A(){
  for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_A[i] == 'V'){
        Kick.write(Kick_down);
        Tom_M.write(Tom_M_down);
        Floor_Tom.write(Floor_down);
        Serial.println(i);
        }
      else if(Beat_LailaMeLaila_A[i] == 'W'){
         Tom_M.write(Tom_M_down);
         Floor_Tom.write(Floor_down);
         Serial.println(i);
         }
         delay(delay_LailaMeLaila);     
      Tom_M.write(Tom_M_up);
      Floor_Tom.write(Floor_up);
      Kick.write(Kick_up);
  }
  Neckmovement();
}
void noteLailaMeLaila_B(){
 for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_B[i] == 'V'){
        Kick.write(Kick_down);
        Tom_M.write(Tom_M_down);
        Floor_Tom.write(Floor_down);
        Serial.println(i);
        }
      else if(Beat_LailaMeLaila_B[i] == 'W'){
         Tom_M.write(Tom_M_down);
         Floor_Tom.write(Floor_down);
         Serial.println(i);
         }
         delay(delay_LailaMeLaila);     
      Tom_M.write(Tom_M_up);
      Floor_Tom.write(Floor_up);
      Kick.write(Kick_up);
  }
  Neckmovement();
}
void noteLailaMeLaila_C(){
 for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_C[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
        }
      else if(Beat_LailaMeLaila_C[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_C[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_C[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_C[i] == 'P'){
         Hihat.write(Hihat_down);
         Snare.write(Snare_down);
         Serial.println(i);
         }  
         delay(delay_LailaMeLaila);
      Snare.write(Snare_up);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_D(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_D[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_D[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_D[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_D[i] == 'P'){
         Hihat.write(Hihat_down);
         Snare.write(Snare_down);
         Serial.println(i);
         }  
         delay(delay_LailaMeLaila);     
      Snare.write(Snare_up);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_E(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_E[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_E[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_E[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_E[i] == 'P'){
         Hihat.write(Hihat_down);
         Snare.write(Snare_down);
         Serial.println(i);
         }  
         delay(delay_LailaMeLaila);
      Snare.write(Snare_up);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_F(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_F[i] == 'H'){
         Tom_H.write(Tom_H_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_F[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_F[i] == 'M'){
         Tom_M.write(Tom_M_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_F[i] == 'F'){
         Floor_Tom.write(Floor_down);
         Serial.println(i);
         } 
         delay(delay_LailaMeLaila);
      Tom_H.write(Tom_H_up);
      Tom_M.write(Tom_M_up);
      Snare.write(Snare_up);
      Floor_Tom.write(Floor_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_G(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_G[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_G[i] == 'K'){
         Kick.write(Kick_down);
         Serial.println(i);
         }
         delay(delay_LailaMeLaila);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_H(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_H[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_H[i] == 'K'){
         Kick.write(Kick_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_H[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      delay(delay_LailaMeLaila);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_I(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_I[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_I[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
         delay(delay_LailaMeLaila);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_J(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_J[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_J[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      delay(delay_LailaMeLaila);
    Kick.write(Kick_up);
    Hihat.write(Hihat_up);
  }
  Neckmovement();
}

void noteLailaMeLaila_K(){
  for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_K[i] == 'V'){
        Kick.write(Kick_down);
        Tom_M.write(Tom_M_down);
        Floor_Tom.write(Floor_down);
        Serial.println(i);
        }
        delay(delay_LailaMeLaila);
      Tom_M.write(Tom_M_up);
      Floor_Tom.write(Floor_up);
      Kick.write(Kick_up);
  }
  Neckmovement();
}
void LailaMeLaila_Track(){
  for(j = 4; j<=112; j++){
    if(j==4 || j==7 || j==10){
      noteLailaMeLaila_A();
    }
    else if(j==5 || j==8 || j==11 || j==13){
      noteLailaMeLaila_K();
    }
    else if(j==12){
      noteLailaMeLaila_B();
    }
    else if(j==14 || j == 22 || j==30 || j == 38 || j==46 || j==81 || j==89 || j==105 || j == 111){
      noteLailaMeLaila_C();
    }
    else if(j == 21 || j==29 || j==37 || j==47 || j==80 || j==90){
      noteLailaMeLaila_E();
    }
    else if(j==45 || j==61 || j==79 || j==88 || j==112 || j==70){
      noteLailaMeLaila_F();
    }
    else if(j==62){
      noteLailaMeLaila_G();
    }
    else if(j>62 && j<70){
      noteLailaMeLaila_H();
    }
    else if(j==71){
      noteLailaMeLaila_I();
    }
    else if(j>71 && j<79){
      noteLailaMeLaila_J();
    }
    else if(j==6 || j==9){
      delay(1724);
    }
    else{
      noteLailaMeLaila_D();
    }
  }
}

void loop(){
  if (Serial.available())
  {
    int state = Serial.parseInt();   
      if (state == 6)
      { 
        Serial.print("Drumm");
        delay(4500);
        LailaMeLaila_Track();
      }
  }
}
