import RPi.GPIO as GPIO                     ## Import GPIO Library.
import time                                 ## Import ‘time’ library for a delay.
 
GPIO.setmode(GPIO.BOARD)                    ## Use BOARD pin numbering.
GPIO.setup(22, GPIO.OUT)                    ## set output.
 
pwm=GPIO.PWM(22,50)                        ## PWM Frequency
pwm.start(2)
 
angle1=0
duty1= float(angle1)/10 + 2.5               ## Angle To Duty cycle  Conversion

angle2=180
duty2= float(angle2)/10 + 2.5
 
ck=0
while ck<=20:
     pwm.ChangeDutyCycle(duty1)
     time.sleep(1)
     pwm.ChangeDutyCycle(duty2)
     time.sleep(1)
     ck=ck+1
time.sleep(1)

GPIO.cleanup()
