#include <Servo.h>
// Song Name :Badtmeez Dil , Movie Name: Yeh Jawaani hai Dewaani , 
int i, j,k;
int delay_BadtmeezDil = 129;
int delay_B = 176;
int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int CrashPin = 10;
int HihatPin = 11;
int SnarePin = 12;
int Snare_up = 95;
int Floor_up = 77;
int Tom_H_up = 80;
int Tom_M_up = 85;
int Crash_up = 90;
int Hihat_up = 90;
int Kick_up = 100;
int Snare_down = 105;
int Floor_down = 67;
int Tom_H_down = 70;
int Tom_M_down = 95;
int Crash_down = 80;
int Hihat_down = 80;
int Kick_down = 115;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Crash;    //C
Servo Hihat;     //T
Servo Kick;     //K
                      // C+K = L   T+K = N   T+S = P  M+T+S+K = Q   M+K = R   S+K = U
char Beat_BadtmeezDil_A[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','0','0'};
char Beat_BadtmeezDil_B[20] = {'K','0','0','S','K','0','S','0','K','0','0','S','K','0','S','0'};
char Beat_BadtmeezDil_C[20] = {'K','0','0','S','K','0','S','0','K','0','0','S','0','0','0','0'};
char Beat_BadtmeezDil_D[20] = {'K','0','S','S','0','K','0','K','S','0','0','0','0','0','0','0'};
char Beat_BadtmeezDil_E[20] = {'K','K','0','S','K','0','S','0','S','S','K','K','S','0','0','0'};
char Beat_BadtmeezDil_F[20] = {'K','0','0','0','S','0','0','0','0','0','K','0','S','0','0','0'};
char Beat_BadtmeezDil_G[20] = {'K','0','0','0','S','0','0','S','0','K','K','0','S','0','0','0'};
char Beat_BadtmeezDil_H[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','S','0'};
char Beat_BadtmeezDil_I[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','S','S'};
char Beat_BadtmeezDil_J[20] = {'K','0','0','0','K','0','0','0','K','0','0','S','S','0','0','0'};


void setup() {
   // put your setup code here, to run once:
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Crash.attach(CrashPin);
  Hihat.attach(HihatPin);
  
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Drummer_ready");
}

void noteBadtmeezDil_A(){
  if(Beat_BadtmeezDil_A[i] == 'K'){
     Kick.write(Kick_down);
     Serial.println(i);
  }
}
void noteBadtmeezDil_B(){
      if(Beat_BadtmeezDil_B[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
        }
      else if(Beat_BadtmeezDil_B[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
        }
}

void noteBadtmeezDil_C(){
        if(Beat_BadtmeezDil_C[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
           }
        else if(Beat_BadtmeezDil_C[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
           }
}
void noteBadtmeezDil_D(){
        if(Beat_BadtmeezDil_D[i] == 'K'){
           Kick.write(Kick_down);
           Serial.println(i);
          }
        else if(Beat_BadtmeezDil_D[i] == 'S'){
           Snare.write(Snare_down);
           Serial.println(i);
          }
}

void noteBadtmeezDil_E(){
        if(Beat_BadtmeezDil_E[i] == 'K'){
           Kick.write(Kick_down);
           Serial.println(i);
            }
         else if(Beat_BadtmeezDil_E[i] == 'S'){
           Snare.write(Snare_down);
           Serial.println(i);
            }
}

void noteBadtmeezDil_F(){
        if(Beat_BadtmeezDil_F[i] == 'K'){
           Kick.write(Kick_down);
           Serial.println(i);
         }
         else if(Beat_BadtmeezDil_F[i] == 'S'){
           Snare.write(Snare_down);
           Serial.println(i);
         }
}

void noteBadtmeezDil_G(){
  if(Beat_BadtmeezDil_G[i] == 'K'){
      Kick.write(Kick_down);
      Serial.println(i);     
    }
  else if(Beat_BadtmeezDil_G[i] == 'S'){
      Snare.write(Snare_down);
      Serial.println(i);
    }
}

void noteBadtmeezDil_H(){
  if(Beat_BadtmeezDil_H[i] == 'K'){
     Kick.write(Kick_down);
     Serial.println(i);
   }
  else if(Beat_BadtmeezDil_H[i] == 'S'){
     Snare.write(Snare_down);
     Serial.println(i);
   }
}

void noteBadtmeezDil_I(){
    if(Beat_BadtmeezDil_I[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
    else if(Beat_BadtmeezDil_I[i] == 'S'){
       Snare.write(Snare_down);
       Serial.println(i);
      }    
}

void noteBadtmeezDil_J(){
      if(Beat_BadtmeezDil_J[i] == 'K'){
          Kick.write(Kick_down);
          Serial.println(i);
        }
      else if(Beat_BadtmeezDil_J[i] == 'S'){
          Snare.write(Snare_down);
          Serial.println(i);
        }      
}
void BeatBadtmeezDil_1(){
for(j = 2; j<=5; j++){ 
     for(i=0; i<=15; i++) {
        if(j == 5){
            noteBadtmeezDil_J();
         }
        else if(j == 4){
            delay_BadtmeezDil = 176;
            noteBadtmeezDil_A();
            }
        else{
            noteBadtmeezDil_A();
        }
        delay(delay_BadtmeezDil);
          Kick.write(Kick_up);
          Snare.write(Snare_up);
          delay_BadtmeezDil = 129;
  }
}
}
//=====================================================
void BeatBadtmeezDil_2(){
  for(j=6; j<= 39; j++){
    for (i=0; i<=15; i++){
        if(j == 13 || j == 31){
            noteBadtmeezDil_C();
        }
        else if(j == 21){
          delay_BadtmeezDil = 176;
           noteBadtmeezDil_C();
        }
        else if(j == 39){
              delay_BadtmeezDil = 176;
              noteBadtmeezDil_E();
          }
        else if(j == 7 || j == 11 || j == 14 || j == 18 || j == 25 || j == 28 || j == 32 || j == 35){
            delay_BadtmeezDil = 176;
            noteBadtmeezDil_B();
        }
          else{
            noteBadtmeezDil_B();
          }
      delay(delay_BadtmeezDil);
      Serial.println(delay_BadtmeezDil);
      Kick.write(Kick_up);
      Snare.write(Snare_up);
      delay_BadtmeezDil = 129;
    }
    }
}
//=======================================================
void BeatBadtmeezDil_4(){
  for(j=40; j<=47; j++){
        for(i=0; i<=15; i++){
            if(j == 43){
                noteBadtmeezDil_G();
              }
              else if(j == 42 || j == 46){
                delay_BadtmeezDil = 176;
                noteBadtmeezDil_F();
              }
            else{
              noteBadtmeezDil_F();
            }
            delay(delay_BadtmeezDil);
            Kick.write(Kick_up);
            Snare.write(Snare_up);
            delay_BadtmeezDil = 129;
      }
  }
}
//=========================================================
void BeatBadtmeezDil_5(){
  for(j=48; j<=55; j++){
      for(i=0; i<=15; i++){
          if(j == 49 || j == 53){
                delay_BadtmeezDil = 176;
                noteBadtmeezDil_H();
            }
           else if(j == 51){
                noteBadtmeezDil_I();
               }
           else {
                  noteBadtmeezDil_A();
                }
      delay(delay_BadtmeezDil);
      Kick.write(Kick_up);
      Snare.write(Snare_up); 
      delay_BadtmeezDil = 129;
      }
  }
}
//============================================================
void BeatBadtmeezDil_6(){
  for(j = 56; j<=79 ; j++){
      for(i=0; i<=15; i++) {
      //Serial.println(i);
          if(j == 61 || j == 71){
              noteBadtmeezDil_D();
          } 
          else if(j == 79){
              noteBadtmeezDil_E();
          }
          else if(j == 56 || j == 60 || j == 63 || j == 70 || j == 74 || j == 77){
              delay_BadtmeezDil = 176;
              noteBadtmeezDil_B();
          }
          else{
              noteBadtmeezDil_B();
          }
        delay(delay_BadtmeezDil);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
        delay_BadtmeezDil = 129;
      }
  }
}
//=========================================================
void BeatBadtmeezDil_9(){
  for(i=0; i<=15; i++) {
      //Serial.println(i);
        
        noteBadtmeezDil_A();
        delay(delay_BadtmeezDil);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
        delay_BadtmeezDil = 129;
     }
  for(j=81; j<=88; j++){
      for(i=0; i<=15; i++){
          if(j == 82 || j == 86){
                noteBadtmeezDil_H();
            }
           else if(j == 84){
                delay_BadtmeezDil = 176;
                noteBadtmeezDil_I();
               }
            else if(j == 81 || j == 88){
                  delay_BadtmeezDil = 176;
                  noteBadtmeezDil_A();
            }
           else {
               noteBadtmeezDil_A();
            }
      delay(delay_BadtmeezDil);
      Kick.write(Kick_up);
      Snare.write(Snare_up); 
      delay_BadtmeezDil = 129;
      }
}
}
//=========================================================================
void BeatBadtmeezDil_10(){
  for(j = 89; j<=102 ; j++){
      for(i=0; i<=15; i++) {
      //Serial.println(i);
          if(j == 94){
              noteBadtmeezDil_D();
          }  
          else if(j == 102){
            delay_BadtmeezDil = 176;
              noteBadtmeezDil_E();
          }
          else if(j == 91 || j == 95 || j == 98){
              delay_BadtmeezDil = 176;
              noteBadtmeezDil_B();
          }
          else{
              noteBadtmeezDil_B();
          }
        delay(delay_BadtmeezDil);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
        delay_BadtmeezDil = 129;
      }
  }
}
void BadtmeezDil_Track(){
  BeatBadtmeezDil_1();
  BeatBadtmeezDil_2();
  BeatBadtmeezDil_4();
  BeatBadtmeezDil_5();
  BeatBadtmeezDil_6();
  BeatBadtmeezDil_9();
  BeatBadtmeezDil_10();
}
//===================================================
void loop()  {
  delay(2000);
  BadtmeezDil_Track();
  Serial.println("THE END");
  delay(1000000000);
}
