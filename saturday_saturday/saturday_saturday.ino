#include <Servo.h>
// Song Name :Saturday Saturday, Movie Name: , 
int i, j,k;
int delay_saturday = 109.375;
int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int NeckPin = 10;
int HihatPin = 11;
int SnarePin = 12;
int Snare_up = 95;
int Floor_up = 70;
int Tom_H_up = 80;
int Tom_M_up = 85;
int Neck_up = 90;
int Hihat_up = 90;
int Kick_up = 90;
int Snare_down = 105;
int Floor_down = 60;
int Tom_H_down = 70;
int Tom_M_down = 95;
int Neck_down = 140;
int Hihat_down = 80;
int Kick_down = 105;
int Current_angle = 90;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Neck;    //C
Servo Hihat;     //T
Servo Kick;     //K
                // C+K = L   T+K = N   T+S = P  M+T+S+K = Q   M+K = R   S+K = U
char Beat_saturday_A[20] = {'K','0','0','S','N','0','S','0','N','0','0','S','N','0','S','0'};  //10
char Beat_saturday_B[20] = {'N','0','0','S','N','0','S','0','N','0','0','S','N','0','S','0'};  //11
char Beat_saturday_C[20] = {'N','0','0','0','N','0','0','0','N','0','0','0','N','0','0','0'};  //17
char Beat_saturday_D[20] = {'Q','0','0','0','Q','0','S','0','N','0','0','0','T','0','0','0'};  //29
char Beat_saturday_E[20] = {'N','0','0','S','N','0','S','0','U','0','0','R','0','0','0','0'};  //92
char Beat_saturday_F[20] = {'0','0','0','0','N','0','0','0','N','0','0','0','N','0','0','0'};  //93


void setup()
{
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Crash.attach(CrashPin);
  Hihat.attach(HihatPin);

  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void Neckmovement(){
  if(Current_angle == Neck_up){
  Neck.write(Neck_down);
  Current_angle = Neck_down;
  }
else if (Current_angle == Neck_down){
  Neck.write(Neck_up);
  Current_angle = Neck_up;
  }
}

void noteSaturday_10(){
    for (i = 0; i <=15; i++) {
      if(Beat_saturday_A[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(Beat_saturday_A[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(Beat_saturday_A[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(delay_saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void noteSaturday_11(){
    for (i = 0; i <=15; i++) {
      if(Beat_saturday_B[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(Beat_saturday_B[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      delay(delay_saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void noteSaturday_17(){
    for (i = 0; i <=15; i++) {
      if(Beat_saturday_C[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void noteSaturday_29(){
    for (i = 0; i <=15; i++) {
      if(Beat_saturday_D[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(Beat_saturday_D[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(Beat_saturday_D[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i); 
      }
      else if(Beat_saturday_D[i] == 'Q'){
        Tom_M.write(Tom_M_down);
        Hihat.write(Hihat_down);
        Snare.write(Snare_down);
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(delay_saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void noteSaturday_92(){
    for (i = 0; i <=15; i++) {
      if(Beat_saturday_E[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(Beat_saturday_E[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(Beat_saturday_E[i] == 'U'){
        Kick.write(Kick_down);
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(Beat_saturday_E[i] == 'R'){
        Kick.write(Kick_down);
        Tom_M.write(Tom_M_down);
        Serial.println(i);
      }
      delay(delay_saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void noteSaturday_93(){
    for (i = 0; i <=15; i++) {
      if(Beat_saturday_F[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void noteSaturday_Track(){
  for(j = 10; j<=93 ; j++){
      if(j == 10 || j == 18 || j == 30 || j == 58 || j == 78){
          noteSaturday_10();
      }
      else if(j == 17 || j == 45){
          noteSaturday_17();
      }
      else if(j == 29 || j == 57 || j == 77){
          noteSaturday_29();
      }
      else if(j == 92){
          noteSaturday_92();
      }
      else if(j == 93){
          noteSaturday_93();
      }
      else{
          noteSaturday_11();    
      }
  }
}
void loop(){
  delay(15750);
  noteSaturday_Track();  
  Serial.println("The end");
  delay(1000000);
}
