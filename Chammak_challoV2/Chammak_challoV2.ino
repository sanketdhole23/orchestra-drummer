#include <Servo.h>
// Song Name : Chammak Challo , Movie Name: Ra.one  
int i, j, k;
int delay_ChammakChallo = 107;
int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int NeckPin = 10;
int HihatPin = 11;
int SnarePin = 12;
int Snare_up = 95;
int Floor_up = 70;
int Tom_H_up = 80;
int Tom_M_up = 85;
int Neck_up = 90;
int Hihat_up = 90;
int Kick_up = 90;
int Snare_down = 105;
int Floor_down = 60;
int Tom_H_down = 70;
int Tom_M_down = 95;
int Neck_down = 140;
int Hihat_down = 80;
int Kick_down = 105;
int Current_angle = 90;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Neck;    //C
Servo Hihat;     //T
Servo Kick;     //K
                // C+K = L   T+K = N   T+S = P  M+T+S+K = Q   M+K = R   S+K = U
char note_Chammak_Challo_A[20] = {'S','0','0','S','S','0','S','0','H','0','H','0','M','0','0','0'};
char note_Chammak_Challo_B[20] = {'L','0','0','0','S','0','S','0','0','0','K','0','S','0','0','0'};
char note_Chammak_Challo_C[20] = {'L','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};
void setup()

{
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Neck.attach(NeckPin);
  Hihat.attach(HihatPin);
  
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void Neckmovement(){
  if(Current_angle == Neck_up){
  Neck.write(Neck_down);
  Current_angle = Neck_down;
  }
else if (Current_angle == Neck_down){
  Neck.write(Neck_up);
  Current_angle = Neck_up;
  }
}

void Chammak_Challo_Fill() {

  for (i = 0; i <=15; i++) {
      if(note_Chammak_Challo_A[i] == 'H'){
        Tom_H.write(Tom_H_down);
        Serial.println(i);
      }
      else if(note_Chammak_Challo_A[i] == 'M'){
        Tom_M.write(Tom_M_down);
        Serial.println(i);
      }
      else if(note_Chammak_Challo_A[i] == 'F'){
        Floor_Tom.write(Floor_down);
        Serial.println(i);
      }
      else if(note_Chammak_Challo_A[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      delay(delay_ChammakChallo);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  }
  Neckmovement(); 
}

void Chammak_Challo_Groove1() {
for(i = 0; i <= 15 ;i++){ 
      if(note_Chammak_Challo_B[i] == 'H'){
        Tom_H.write(Tom_H_down);
        Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'M'){
        Tom_M.write(Tom_M_down);
        Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'F'){
        Floor_Tom.write(Floor_down);
        Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'L'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(delay_ChammakChallo);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  }
  Neckmovement(); 
}

void Chammak_Challo_Groove3(){
  for(j=0; j<3; j++){  
      Chammak_Challo_Groove1();
  }
}

void Chammak_Challo_Groove4(){
  for(j=0; j<4; j++){  
      Chammak_Challo_Groove1();
  }
}
void Chammak_Challo_Groove7(){
      Chammak_Challo_Groove3();
      Chammak_Challo_Groove4();
}

void Chammak_Challo_Blank(){
  for(i = 0; i<=15 ; i++){
      if(note_Chammak_Challo_C[i] == 'L'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
    delay(delay_ChammakChallo);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  }
  Neckmovement();
}

void Chammak_Challo_Fill_Groove7(){
  Chammak_Challo_Fill();
  Chammak_Challo_Groove7();
}

void Chammak_Challo_Fill_Groove3(){
  Chammak_Challo_Fill();
  Chammak_Challo_Groove3();
}
void Chammak_Challo_Blank_Groove3(){
  Chammak_Challo_Blank();
  Chammak_Challo_Groove3();
}

void Chammak_Challo_Blank_Groove7(){
  Chammak_Challo_Blank();
  Chammak_Challo_Groove7();
}

void loop() {


Chammak_Challo_Fill_Groove7();
Chammak_Challo_Fill_Groove7();
Chammak_Challo_Blank_Groove3();
Chammak_Challo_Blank_Groove7();
Chammak_Challo_Fill_Groove7();
Chammak_Challo_Fill_Groove7();
Chammak_Challo_Fill_Groove7();
Chammak_Challo_Blank_Groove3();
Chammak_Challo_Blank_Groove7();
Chammak_Challo_Fill_Groove7();
Chammak_Challo_Fill_Groove7();
Chammak_Challo_Blank_Groove7();
Chammak_Challo_Groove4();
Chammak_Challo_Fill_Groove7();
Chammak_Challo_Fill_Groove7();
Chammak_Challo_Fill_Groove7();
Chammak_Challo_Fill_Groove7();
Chammak_Challo_Fill();

Serial.println("The end");
delay(10000000);
}
