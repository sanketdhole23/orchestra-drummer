#include <Servo.h>
// Song Name :Saturday Saturday, Movie Name: , 
int i, j,k;
int delay_BalamPichkari = 125;
int delay_Saturday = 109.375;
int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int NeckPin = 10;
int HihatPin = 11;
int SnarePin = 12;
int Snare_up = 95;
int Floor_up = 70;
int Tom_H_up = 80;
int Tom_M_up = 85;
int Neck_up = 90;
int Hihat_up = 90;
int Kick_up = 90;
int Snare_down = 105;
int Floor_down = 60;
int Tom_H_down = 70;
int Tom_M_down = 95;
int Neck_down = 140;
int Hihat_down = 80;
int Kick_down = 105;
int Current_angle = 90;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Neck;    //C
Servo Hihat;     //T
Servo Kick;     //K
                // C+K = L   T+K = N   T+S = P  M+T+S+K = Q   M+K = R   S+K = U
char note_Saturday_AS[20] = {'N','0','0','S','N','0','S','0','N','0','0','S','N','0','S','0'};  //10
char note_Saturday_BS[20] = {'N','0','0','S','N','0','S','0','N','0','0','S','N','0','S','0'};  //11
char note_Saturday_CS[20] = {'N','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};  //17
char note_Saturday_DS[20] = {'V','0','0','0','V','0','0','0','0','0','0','0','0','0','0','0'};  //29
char note_Saturday_ES[20] = {'N','0','0','S','N','0','S','0','U','0','0','R','0','0','0','0'};  //92
char note_Saturday_FS[20] = {'0','0','0','0','N','0','0','0','N','0','0','0','N','0','0','0'};  //93
char note_Saturday_GS[20] = {'T','0','0','S','T','0','S','0','0','0','0','0','0','0','0','0'};  //49
char note_Saturday_HS[20] = {'T','0','0','S','T','0','S','0','T','0','0','S','T','0','S','0'};  //46

//Balam Pichkari
char note_BalamPichkari_A[20] = {'N','S','T','S','T','0','0','0','0','0','0','0','0','0','K','0'};  //7
char note_BalamPichkari_B[20] = {'N','S','T','S','T','0','K','0','N','S','T','S','T','0','K','0'};  //11
char note_BalamPichkari_C[20] = {'N','S','T','S','T','0','K','0','K','0','0','0','0','0','0','0'};  //14
char note_BalamPichkari_D[20] = {'N','0','T','S','T','0','N','0','N','0','T','S','T','0','N','0'};  //15
char note_BalamPichkari_E[20] = {'N','0','T','S','T','0','N','0','K','0','0','K','0','0','K','0'};  //39
char note_BalamPichkari_F[20] = {'N','0','T','K','0','0','K','0','S','0','0','M','0','0','K','0'};  //68
char note_BalamPichkari_G[20] = {'N','0','T','S','T','0','N','0','N','S','T','S','K','0','0','0'};  //80
char note_BalamPichkari_H[20] = {'N','0','T','S','T','0','T','0','N','S','T','S','T','S','N','0'};  //96
char note_BalamPichkari_I[20] = {'K','0','0','S','0','0','K','0','N','0','T','S','T','0','N','0'};  //81

void setup()
{
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Neck.attach(NeckPin);
  Hihat.attach(HihatPin);
  
  pinMode(13, OUTPUT);
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Drummer_ready");
}
//======================================================
void BeatSaturday_10(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_AS[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Saturday_AS[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Saturday_AS[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_11(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_BS[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Saturday_BS[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_17(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_CS[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_29(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_DS[i] == 'V'){
        Kick.write(Kick_down);
        Snare.write(Snare_down);
        Floor_Tom.write(Floor_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_92(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_ES[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Saturday_ES[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Saturday_ES[i] == 'U'){
        Kick.write(Kick_down);
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Saturday_ES[i] == 'R'){
        Kick.write(Kick_down);
        Tom_M.write(Tom_M_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_46(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_HS[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Saturday_HS[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_49(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_GS[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Saturday_GS[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void Saturday_Track(){
  for(j = 10; j<=92 ; j++){
      if(j == 10 || j == 18 || j == 30 || j == 58 || j == 78){
          BeatSaturday_10();
      }
      else if(j == 17 || j==26 || j==27 || j==28 || j == 45 || j==54 || j==55 || j==56 || j==74 || j==75 || j==76 ){
          BeatSaturday_17();
      }
      else if(j == 29 || j == 57 || j == 77){
          BeatSaturday_29();
      }
      else if(j == 49 || j == 69){
        BeatSaturday_49();
      }
      else if(j == 92){
          BeatSaturday_92();
      }
      else if(j == 46 || j==47 || j==48 || j==65 || j==66 || j==67 || j==68 || j==85){
          BeatSaturday_46();
      }
      else{
          BeatSaturday_11();    
      }
  }
}
//==========================================================================

void BeatBalamPichkari_7() {
for(j = 7; j<=10 ; j++){
  for (i = 0; i <=15; i++) {
      if(note_BalamPichkari_A[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_BalamPichkari_A[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_BalamPichkari_A[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_BalamPichkari_A[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}
}

//11
void BeatBalamPichkari_11() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_B[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_B[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_B[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_B[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

//14
void BeatBalamPichkari_14() {
  for (i = 0; i <=15; i++){
          if(note_BalamPichkari_C[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_C[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_C[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_C[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
    Tom_H.write(Tom_H_up);
    Tom_M.write(Tom_M_up);
    Snare.write(Snare_up);
    Floor_Tom.write(Floor_up);
    Kick.write(Kick_up);
    Hihat.write(Hihat_up);
    }
}

//15
void BeatBalamPichkari_15() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_D[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_D[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_D[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_D[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

//39
void BeatBalamPichkari_39() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_E[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_E[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_E[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_E[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatBalamPichkari_68() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_F[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_F[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_F[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_F[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatBalamPichkari_80() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_G[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_G[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_G[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_G[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatBalamPichkari_81() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_I[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_I[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_I[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_I[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}
void BeatBalamPichkari_96() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_H[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_H[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_H[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_H[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatBalamPichkari_Track(){
  for(k = 11; k<=97; k++){
    if(k == 14 || k == 51){
      BeatBalamPichkari_14();
    }
    else if(k == 39){
      BeatBalamPichkari_39();
    }
    else if(k == 68){
      BeatBalamPichkari_68();
    }
    else if(k == 80){
      BeatBalamPichkari_80();
    }
    else if(k == 81){
      BeatBalamPichkari_81();
    }
    else if(k == 96){
      BeatBalamPichkari_96();
    }
    else  {
      BeatBalamPichkari_11();
    }
  }
}


void loop(){
  if (Serial.available())
  {
    int state = Serial.parseInt();
    Serial.println(state);
    Serial.println("Drummer_ready");
      if (state == 4)
      {
        delay(11000);
        BeatBalamPichkari_7();
        BeatBalamPichkari_Track();
      }
      else if(state == 5){
        delay(15750);
        Saturday_Track();  
        Serial.println("The end");
      }
  }
}
