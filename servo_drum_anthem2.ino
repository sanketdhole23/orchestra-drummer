#include <Servo.h> 

int i,j;
int servoPin1 = 3;
int servoPin2 = 4;
Servo Hihat;
Servo Bass;

void setup() {
   // put your setup code here, to run once:
  Hihat.attach(servoPin1);
  Bass.attach(servoPin2);
  
}

void beat1(){
  for (i = 0; i<24; i++){
  Hihat.write(0);
  Bass.write(0);
  delay(500);
  Hihat.write(45);
  Bass.write(45);
  delay(500);  
  }
  
}

void beat2(){
  for (i = 0; i<7; i++){
  Hihat.write(0);
  Bass.write(0);
  delay(500);
  Hihat.write(45);
  Bass.write(45);
  delay(500);  
  }
  
}

void beat3() {
    
  for(i=0; i<3; i++) {

    for(j=0; j<2; j++){
      
      Hihat.write(0);
      Bass.write(0);
      delay(250);
      Hihat.write(45);
      Bass.write(45);
      delay(250);  
    }
    delay(500);
  }
}

void beat4() {
  
  for(i=0; i<3; i++) {
    
  Hihat.write(0);
  Bass.write(0);
  delay(500);
  Hihat.write(45);
  Bass.write(45);
  delay(500);  
  }
  
}

void loop()  {

  beat1();
  delay(12000);
  beat2();
  delay(1000);  
  beat3();
  beat4();
  delay(3000);
  
}

