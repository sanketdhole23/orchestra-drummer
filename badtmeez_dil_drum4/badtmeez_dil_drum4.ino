#include <Servo.h> 

int i,j;

int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int CrashPin = 10;
int RidePin = 11;
int SnarePin = 12;
int Snare_up;
int Floor_up;
int Tom_H_up;
int Tom_M_up;
int Crash_up;
int Ride_up;
int Kick_up;
int Snare_down;
int Floor_down;
int Tom_H_down;
int Tom_M_down;
int Crash_down;
int Ride_down;
int Kick_down;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Crash;    //C
Servo Ride;     //R
Servo Kick;     //K
Servo neck;
char note_A[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','0','0'};
char note_A[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','','0','0','0'};
char note_B[20] = {'K','0','0','S','K','0','S','0','K','0','S','0','K','0','S','0'};
char note_C[20] = {'K','0','0','S','K','0','S','0','K','0','S','0','0','0','0','0'};
char note_D[20] = {'K','0','S','S','0','K','0','K','S','0','0','0','0','0','0','0'};
char note_E[20] = {'K','K','0','S','K','0','S','0','S','S','K','K','S','0','0','0'};
char note_F[20] = {'K','0','0','0','S','0','0','0','0','0','K','0','S','0','0','0'};
char note_G[20] = {'K','0','0','0','S','0','0','S','0','K','K','0','S','0','0','0'};
char note_H[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','S','0'};
char note_I[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','S','S'};
char note_J[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','0','0','0','0'};
Servo Kick;
Servo Snare;

void setup() {
   // put your setup code here, to run once:
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Crash.attach(CrashPin);
  Ride.attach(RidePin);
  neck.attach(servopin5);
  Serial.begin(9600);
}

void noteB(){
      if(note_B[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
        }
      else if(note_B[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
          }
}

void noteD(){
        if(note_D[i] == 'K'){
           Kick.write(Kick_down);
           Serial.println(i);
          }
        else if(note_D[i] == 'S'){
           Snare.write(Snare_down);
           Serial.println(i);
          }
}

void noteE(){
        if(note_E[i] == 'K'){
           Kick.write(Kick_down);
           Serial.println(i);
            }
        
         else if(note_E[i] == 'S'){
           Snare.write(Snare_down);
           Serial.println(i);
            }
}
void beat1(){
for(j = 0; j<=4; j++){ 
    for(i=0; i<=15; i++) {
        if(note_A[i] == 'K'){
          Kick.write(Kick_down);
          Serial.println(i);
           }
        delay(142);
          Kick.write(Kick_up);
          Snare.write(Snare_up);
      }
  }
}

//=====================================================
void beat2(){
  for(j=1; j<= 34; j++){
    for (i=0; i<=15; i++){
        if(j == 8 || j == 16 || j == 26){
            if(note_C[i] == 'K'){
              Kick.write(Kick_down);
              Serial.println(i);
              }
        
            else if(note_C[i] == 'S'){
              Snare.write(Snare_down);
              Serial.println(i);
            }
        }
        else if(j == 34){
              noteE();
          }
        else {
          noteB();
        }
      delay(142);
      Kick.write(Kick_up);
      Snare.write(Snare_up);
    }
  }
}
//=======================================================
void beat4(){
  for(j=1; j<=8; j++){
        for(i=0; i<=15; i++){
            if(j == 4){
                if(note_G[i] == 'K'){
                    Kick.write(Kick_down);
                    Serial.println(i);     
                    }
                else if(note_G[i] == 'S'){
                   Snare.write(Snare_down);
                   Serial.println(i);
                    }
              }
            else{
              if(note_F[i] == 'K'){
                Kick.write(Kick_down);
                Serial.println(i);
                }
              else if(note_F[i] == 'S'){
                 Snare.write(Snare_down);
                 Serial.println(i);
                  }
            }
            delay(142);
            Kick.write(Kick_up);
            Snare.write(Snare_up);
      }
  }
}
//=========================================================
void beat5(){
  for(j=1; j<=8; j++){
 
      for(i=0; i<=15; i++){
          if(j == 2 || j == 6){
                if(note_H[i] == 'K'){
                    Kick.write(Kick_down);
                    Serial.println(i);
                    }
                else if(note_H[i] == 'S'){
                    Snare.write(Snare_down);
                    Serial.println(i);
                    }    
            }
           else if(j == 4){
                if(note_I[i] == 'K'){
                   Kick.write(Kick_down);
                   Serial.println(i);
                   }
                else if(note_I[i] == 'S'){
                   Snare.write(Snare_down);
                   Serial.println(i);
                   }    
               }
           else {
                  if(note_A[i] == 'K'){
                  Kick.write(Kick_down);
                  Serial.println(i);
                  }
            }
      delay(142);
      Kick.write(Kick_up);
      Snare.write(Snare_up); 
      }
  }
}
//============================================================
void beat6(){
  for(j = 1; j<=24 ; j++){
      for(i=0; i<=15; i++) {
      //Serial.println(i);
          if(j == 6 || j == 16){
              noteD();
          } 
          else if(j == 24){
              noteE();
          }
          else{
              noteB();
          }
        delay(142);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
      }
  }
}
//=========================================================
void beat9(){
  for(i=0; i<=15; i++) {
      //Serial.println(i);
        if(note_A[i] == 'K'){
         Kick.write(Kick_down);
         Serial.println(i);
        }
        delay(142);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
     }
  beat5();
}
//=========================================================================
void beat10(){
  for(j = 1; j<=14 ; j++){
      for(i=0; i<=15; i++) {
      //Serial.println(i);
          if(j == 6){
              noteD();
          }
          else if(j == 14){
              noteE();
          }
          else{
              noteB();
          }
        delay(142);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
      }
  }
}

//===================================================
void loop()  {
 
  beat1();
  beat2();
  beat4();
  beat5();
  beat6();
  beat9();
  beat10();
  Serial.println("THE END");
  delay(1000000000000);
}

