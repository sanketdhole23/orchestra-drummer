#include <Servo.h>
// Song Name : , Movie Name: , 
int i, j;
int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int CrashPin = 10;
int RidePin = 11;
int SnarePin = 12;
int Snare_up;
int Floor_up;
int Tom_H_up;
int Tom_M_up;
int Crash_up;
int Ride_up;
int Kick_up;
int Snare_down;
int Floor_down;
int Tom_H_down;
int Tom_M_down;
int Crash_down;
int Ride_down;
int Kick_down;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Crash;    //C
Servo Ride;     //R
Servo Kick;     //K
Servo neck;
                //C+K = L
char note_A[20] = {'S','0','0','S','S','0','S','0','H','0','H','0','M','0','0','0'};
char note_B[20] = {'L','0','0','0','S','0','S','0','0','0','K','0','S','0','0','0'};
char note_C[20] = {'L','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};
void setup()

{
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Crash.attach(CrashPin);
  Ride.attach(RidePin);

  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void Fill() {

  for (i = 0; i <=15; i++) {
      if(note_A[i] == 'H'){
        Tom_H.write(Tom_H_down);
        Serial.println(i);
      }
      else if(note_A[i] == 'M'){
        Tom_M.write(Tom_M_down);
        Serial.println(i);
      }
      else if(note_A[i] == 'F'){
        Floor_Tom.write(Floor_down);
        Serial.println(i);
      }
      else if(note_A[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  } 
}

void Groove1() {
for(i = 0; i <= 15 ;i++){ 
      if(note_B[i] == 'H'){
        Tom_H.write(Tom_H_down);
        Serial.println(i);
      }
      else if(note_B[i] == 'M'){
        Tom_M.write(Tom_M_down);
        Serial.println(i);
      }
      else if(note_B[i] == 'F'){
        Floor_Tom.write(Floor_down);
        Serial.println(i);
      }
      else if(note_B[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_B[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      else if(note_B[i] == 'L'){
        Crash.write(Crash_down);
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  } 
}

void Groove3(){
  for(j=0; j<3; j++){  
      Groove1();
  }
}

void Groove4(){
  for(j=0; j<4; j++){  
      Groove1();
  }
}
void Groove7(){
      Groove3();
      Groove4();
}

void Blank(){
  for(i = 0; i<=15 ; i++){
      if(note_B[i] == 'L'){
        Crash.write(Crash_down);
        Kick.write(Kick_down);
        Serial.println(i);
      }
    delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  }
}

void Fill_Groove7(){
  Fill();
  Groove7();
}

void Fill_Groove3(){
  Fill();
  Groove3();
}
void Blank_Groove3(){
  Blank();
  Groove3();
}

void Blank_Groove7(){
  Blank();
  Groove7();
}

void loop() {

Blank_Groove7();
delay(20000000);
/*
Fill_Groove7();
Fill_Groove7();
Fill_Groove3();
Blank_Groove3();
Fill_Groove7();
Fill_Groove7();
Fill_Groove7();
Blank_Groove3();
Blank_Groove7();
Fill_Groove7();
Fill_Groove7();
Blank_Groove7();
Groove4();
Fill_Groove7();
Fill_Groove7();
Fill_Groove7();
Fill_Groove7();
Fill();
*/
}
