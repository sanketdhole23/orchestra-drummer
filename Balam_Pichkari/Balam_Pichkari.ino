

#include <Servo.h>
// Song Name :Balam Pichkari , Movie Name: Dhoom , 
int i, j,k;
int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int NeckPin = 10;
int HihatPin = 11;
int SnarePin = 12;
int Snare_up = 95;
int Floor_up = 70;
int Tom_H_up = 80;
int Tom_M_up = 85;
int Neck_up = 90;
int Hihat_up = 90;
int Kick_up = 90;
int Snare_down = 105;
int Floor_down = 60;
int Tom_H_down = 70;
int Tom_M_down = 95;
int Neck_down = 140;
int Hihat_down = 80;
int Kick_down = 105;
int Current_angle = 90;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Neck;    //C
Servo Hihat;     //T
Servo Kick;     //K
                // C+K = L   T+K = N   T+S = P  M+T+S+K = Q   M+K = R   S+K = U
char Beat_BalamPichkari_A[20] = {'N','S','T','S','T','0','0','0','0','0','0','0','0','0','K','0'};  //7
char Beat_BalamPichkari_B[20] = {'N','S','T','S','T','0','K','0','N','S','T','S','T','0','K','0'};  //11
char Beat_BalamPichkari_C[20] = {'N','S','T','S','T','0','K','0','K','0','0','0','0','0','0','0'};  //14
char Beat_BalamPichkari_D[20] = {'N','0','T','S','T','0','N','0','N','0','T','S','T','0','N','0'};  //15
char Beat_BalamPichkari_E[20] = {'N','0','T','S','T','0','N','0','K','0','0','K','0','0','K','0'};  //39
char Beat_BalamPichkari_F[20] = {'N','0','T','K','0','0','K','0','S','0','0','M','0','0','K','0'};  //68
char Beat_BalamPichkari_G[20] = {'N','0','T','S','T','0','N','0','N','S','T','S','K','0','0','0'};  //80
char Beat_BalamPichkari_H[20] = {'N','0','T','S','T','0','T','0','N','S','T','S','T','S','N','0'};  //96
char Beat_BalamPichkari_I[20] = {'K','0','0','S','0','0','K','0','N','0','T','S','T','0','N','0'};  //81

void setup()
{
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Crash.attach(CrashPin);
  Hihat.attach(HihatPin);

  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void Neckmovement(){
  if(Current_angle == Neck_up){
  Neck.write(Neck_down);
  Current_angle = Neck_down;
  }
else if (Current_angle == Neck_down){
  Neck.write(Neck_up);
  Current_angle = Neck_up;
  }
}

//7
void noteBalamPichkari_7() {
for(j = 7; j<=10 ; j++){
  for (i = 0; i <=15; i++) {
      if(Beat_BalamPichkari_A[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(Beat_BalamPichkari_A[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(Beat_BalamPichkari_A[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(Beat_BalamPichkari_A[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}
}

//11
void noteBalamPichkari_11() {
  for (i = 0; i <=15; i++) {             
          if(Beat_BalamPichkari_B[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_B[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_B[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_B[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

//14
void noteBalamPichkari_14() {
  for (i = 0; i <=15; i++){
          if(Beat_BalamPichkari_C[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_C[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_C[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_C[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(125);
    Tom_H.write(Tom_H_up);
    Tom_M.write(Tom_M_up);
    Snare.write(Snare_up);
    Floor_Tom.write(Floor_up);
    Crash.write(Crash_up);
    Kick.write(Kick_up);
    Hihat.write(Hihat_up);
    }
}

//15
void noteBalamPichkari_15() {
  for (i = 0; i <=15; i++) {             
          if(Beat_BalamPichkari_D[i] == 'N'){
            Kick.write(Kick_down);     
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_D[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_D[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_D[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

//39
void noteBalamPichkari_39() {
  for (i = 0; i <=15; i++) {             
          if(Beat_BalamPichkari_E[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_E[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_E[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_E[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void noteBalamPichkari_68() {
  for (i = 0; i <=15; i++) {             
          if(Beat_BalamPichkari_F[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_F[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_F[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_F[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void noteBalamPichkari_80() {
  for (i = 0; i <=15; i++) {             
          if(Beat_BalamPichkari_G[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_G[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_G[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_G[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void noteBalamPichkari_81() {
  for (i = 0; i <=15; i++) {             
          if(Beat_BalamPichkari_I[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_I[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_I[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_I[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}
void noteBalamPichkari_96() {
  for (i = 0; i <=15; i++) {             
          if(Beat_BalamPichkari_H[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_H[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_H[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(Beat_BalamPichkari_H[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(125);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Crash.write(Crash_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void noteBalamPichkari_Track(){
  for(k = 11; k<=97; k++){
    if(k == 14 || k == 51){
      noteBalamPichkari_14();
    }
    else if(k == 39){
      noteBalamPichkari_39();
    }
    else if(k == 68){
      noteBalamPichkari_68();
    }
    else if(k == 80){
      noteBalamPichkari_80();
    }
    else if(k == 81){
      noteBalamPichkari_81();
    }
    else if(k == 96){
      noteBalamPichkari_96();
    }
    else  {
      noteBalamPichkari_11();
    }
  }
}

void loop(){
  noteBalamPichkari_7();
  noteBalamPichkari_Track();
  Serial.println("The end");
}
