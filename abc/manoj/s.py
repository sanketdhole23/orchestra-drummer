import RPi.GPIO as GPIO
import  time
GPIO.setmode(GPIO.BOARD)
GPIO.setup(22,GPIO.OUT)
p= GPIO.PWM(22,50)
p.start(2)
try:
    while true:
        p.ChangeDutyCycle(2.5)
        time.sleep(1)
        p.ChangeDutyCycle(7.5)
        time.sleep(1)
        p.ChangeDutyCycle(12.5)
        time.sleep(1)
except keyboardInterrupt:
   
    GPIO.cleanup()
    
