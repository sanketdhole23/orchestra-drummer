#include <Servo.h>
// Program for National Anthem
int i, j;

int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int CrashPin = 10;
int HihatPin = 11;
int SnarePin = 12;
int Snare_up = 95;
int Floor_up = 77;
int Tom_H_up = 80;
int Tom_M_up = 85;
int Crash_up = 90;
int Hihat_up = 90;
int Kick_up = 100;
int Snare_down = 105;
int Floor_down = 67;
int Tom_H_down = 70;
int Tom_M_down = 95;
int Crash_down = 80;
int Hihat_down = 80;
int Kick_down = 115;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Crash;    //C
Servo Hihat;     //T
Servo Kick;     //K

char note_A[20] = {'H','M','F','0','S','0','H','F'};
char note_B[20] = {'S','H','M','F'};
void setup(){
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Crash.attach(CrashPin);
  Hihat.attach(HihatPin);
  
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Drummer_ready");
}

void beat1() {
for(j = 0; j<=3; j++){
  for (i = 0; i <=7; i++) {
      if(note_A[i] == 'H'){
        Tom_H.write(Tom_H_down);
        Serial.println(i);
      }
      else if(note_A[i] == 'M'){
        Tom_M.write(Tom_M_down);
        Serial.println(i);
      }
      else if(note_A[i] == 'F'){
        Floor_Tom.write(Floor_down);
        Serial.println(i);
      }
      else if(note_A[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      delay(250);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  } 
}
}
void beat2() {
  
for(int k = 0; k < 3;k++){ 
  beat1();
}
}

void beat3() {

   for (i = 0; i <4; i++) {
      if(note_B[i] == 'H'){
        Tom_H.write(Tom_H_down);
        Serial.println(i);
      }
      else if(note_B[i] == 'M'){
        Tom_M.write(Tom_M_down);
        Serial.println(i);
      }
      else if(note_B[i] == 'F'){
        Floor_Tom.write(Floor_down);
        Serial.println(i);
      }
      else if(note_B[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      delay(500);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  }                        
}


void loop() {
beat2();
Serial.println("/////beat1 finish/////");
delay(12000);
beat1();
Serial.println("/////beat2 finish/////");
delay(6000);
beat3();
Serial.println("/////The end/////");
delay(200000);
/*
  while (Serial.available())

  {
    int state = Serial.parseInt();
    Serial.println(state);
    Serial.println("qwerty");
    Serial.flush();

    if (state == 1)
    {
      Serial.println("stage 1");
      digitalWrite(13, HIGH);
      beat1();
      delay(12000);
      beat2();
      delay(6000);
      beat3();
      delay(3000);
      //Serial.println("Command completed LED turned ON");
    }

    if (state == 2)

    {
      Serial.print("abc");
      digitalWrite(13, LOW);

      // Tom_L.write(100);
      //    Tom_R.write(80);
      Tom_L.detach();
      Tom_R.detach();
      Floor_Tom.detach();
      Snare.detach();
      neck.detach();
      //Serial.println("Command completed LED turned OFF");
    }
  }

*/

}
