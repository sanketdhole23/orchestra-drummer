#include <Servo.h> 

//
int i,j;
int servoPin1 = 11;
int servoPin2 = 10;
int Kick_up = 180;
int Kick_down =150 ;
int Snare_up = 0 ;
int Snare_down = 30 ;
Servo Kick;
Servo Snare;

void setup() {
   // put your setup code here, to run once:
  Kick.attach(servoPin1);
  Snare.attach(servoPin2);
  Serial.begin(9600);
}

void beat1(){

  for(i=1; i<=16; i++) {
    //Serial.println(i);
      if(i%4 == 1){
       Kick.write(Kick_down);
       Serial.println(i);
      }
      delay(125);
      Kick.write(Kick_up);
      Snare.write(Snare_up);
   }
}
//===================================================
void beat2() {
    
  for(i=1; i<=16; i++) {
    //Serial.println(i);
      if(i == 1 || i == 5 || i == 9 || i == 13){
       Kick.write(Kick_down);
       Serial.println(i);
      }
      
       else if(i == 4 || i == 7 || i == 11 || i == 15)
       {
        Snare.write(Snare_down);
        Serial.println(i);
       }
  
      delay(125);
      Kick.write(Kick_up);
      Snare.write(Snare_up);
   }
}

//=====================================================

void beat3(){
  for(j = 1; j <= 24; j++){
    for (i=1; i<=16; i++){
        if(j == 8){
            if(i == 1 || i == 5 || i == 9){
              Kick.write(Kick_down);
              Serial.println(i);
            }
        
            else if(i == 4 || i == 7 || i == 11){
              Snare.write(Snare_down);
              Serial.println(i);
            }
        }
        else {
          beat2();
        }
      delay(125);
      Kick.write(Kick_up);
      Snare.write(Snare_up);
    }
  }
}
//=====================================================

void beat4(){
  for(j = 1; j<=6 ; j++){
      for(i=1; i<=16; i++) {
      //Serial.println(i);
          if(j<6){
              if(i == 1 || i == 6 || i == 8){
                  Kick.write(Kick_down);
                  Serial.println(i);
              }
        
              else if(i == 3 || i == 4 || i == 9){
                  Snare.write(Snare_down);
                  Serial.println(i);
              }
          }
          else{
            beat2();
          }
        delay(125);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
      }
  }
}

//=====================================================

void beat5(){
   for(j = 1; j<=8 ; j++){
      for(i=1; i<=16; i++) {
          if(j == 8){
              if(i == 1 || i == 2 || i == 11 || i == 12){
                  Kick.write(Kick_down);
                  Serial.println(i);
              }
        
               else if(i == 4 || i == 7 || i == 9 || i == 13){
                  Snare.write(Snare_down);
                  Serial.println(i);
                }
          }
          else{
            beat2();
          }
          delay(125);
          Kick.write(Kick_up);
          Snare.write(Snare_up);
     }
   }
}
//=======================================================

void beat7(){
  for(j=1; j<=8; j++){
 
        for(i=1; i<=16; i++){
            if(j == 4){
                if(i == 1 || i == 10 || i == 11){
                    Kick.write(Kick_down);
                  Serial.println(i);     
                }
                else if(i == 5 || i == 8 || i == 13){
                  Snare.write(Snare_down);
                   Serial.println(i);
                }
            }
            else{
              if(i == 1 || i == 11){
                Kick.write(Kick_down);
                Serial.println(i);
              }
              else if( i == 5 || i == 13){
                Snare.write(Snare_down);
                 Serial.println(i);
              }
            }
            delay(125);
            Kick.write(Kick_up);
            Snare.write(Snare_up);
      }
  }
}

//=========================================================
void beat8(){
  for(j=1; j<=8; j++){
 
      for(i=1; i<=16; i++){
          if(j == 2 || j == 6){
                if(i == 1 || i == 5 || i == 9 || i == 13){
                  Kick.write(Kick_down);
                    Serial.println(i);
                }
                else if(i == 15){
                    Snare.write(Snare_down);
                     Serial.println(i);
                }    
            }
           else if(j == 4){
                if(i == 1 || i == 5 || i == 9 || i == 13){
                   Kick.write(Kick_down);
                   Serial.println(i);
                 }
                 else if(i == 15 || i == 16){
                   Snare.write(Snare_down);
                   Serial.println(i);
                   }    
              }   
           else {
                  beat1();
            }
      delay(125);
      Kick.write(Kick_up);
      Snare.write(Snare_up); 
      }
  }
}
//=========================================================
void beat9(){
  for(j=1; j<=9; j++){
      for(i = 0; i <=16 ; i++){
         if(j>4){
          
         }
         else {
            beat2();
         }
        delay(125);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
      }
  }
}
//=========================================================================


void loop()  {
  beat3();
  beat4();
  beat5();
  beat7();
  beat8();
  beat4();
  beat5();
  beat5();
  Serial.println("THE END");
  
}

