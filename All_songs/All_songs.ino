#include <Servo.h>
//#include <drum_head.h>
// Song Name :Badtmeez Dil , Movie Name: Yeh Jawaani hai Dewaani , 
int i, j,k;
int delay_BadtmeezDil = 143.25;
int delay_ChammakChallo = 107;
int delay_Zingaat = 125;
int delay_Dhoom = 143;
int delay_LailaMeLaila = 107.79;
int delay_BalamPichkari = 125;
int delay_Saturday = 110.880;

int TomHPin = 6;    //
int TomMPin = 7;      //
int FloorPin = 8;      //
int KickPin = 9;
int NeckPin = 10;
int HihatPin = 11;
int SnarePin = 12;

int Snare_up = 109;
int Floor_up = 60;
int Tom_H_up = 70;
int Tom_M_up = 95;
int Neck_up = 90;
int Hihat_up = 90;
int Kick_up = 10;
int Snare_down = 119;
int Floor_down = 50;
int Tom_H_down = 60;
int Tom_M_down = 110;
int Neck_down = 140;
int Hihat_down = 80;
int Kick_down = 25;
int Current_angle = 90;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Neck;    //C
Servo Hihat;     //T
Servo Kick;     //K
                    // C+K = L   T+K = N   T+S = P  M+T+S+K = Q   M+K = R   S+K = U  K+S+F = V
char Beat_BadtmeezDil_A[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','0','0'};
char Beat_BadtmeezDil_B[20] = {'K','0','0','S','K','0','S','0','K','0','0','S','K','0','S','0'};
char Beat_BadtmeezDil_C[20] = {'K','0','0','S','K','0','S','0','K','0','0','S','0','0','0','0'};
char Beat_BadtmeezDil_D[20] = {'K','0','S','S','0','K','0','K','S','0','0','0','0','0','0','0'};
char Beat_BadtmeezDil_E[20] = {'K','K','0','S','K','0','S','0','S','S','K','K','S','0','0','0'};
char Beat_BadtmeezDil_F[20] = {'K','0','0','0','S','0','0','0','0','0','K','0','S','0','0','0'};
char Beat_BadtmeezDil_G[20] = {'K','0','0','0','S','0','0','S','0','K','K','0','S','0','0','0'};
char Beat_BadtmeezDil_H[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','S','0'};
char Beat_BadtmeezDil_I[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','S','S'};
char Beat_BadtmeezDil_J[20] = {'K','0','0','0','K','0','0','0','K','0','0','S','S','0','0','0'};

char note_Chammak_Challo_A[20] = {'S','0','0','S','S','0','S','0','H','0','H','0','M','0','0','0'};
char note_Chammak_Challo_B[20] = {'L','0','0','0','S','0','S','0','0','0','K','0','S','0','0','0'};
char note_Chammak_Challo_C[20] = {'L','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};

char note_Dhoom_A[20] = {'N','0','T','0','N','0','T','0','N','0','T','0','N','0','T','0'};
char note_Dhoom_B[20] = {'N','0','N','0','N','0','N','0','N','0','N','0','N','0','N','0'};
char note_Dhoom_C[20] = {'N','0','P','K','T','K','P','0','N','0','P','0','N','0','P','0'};
char note_Dhoom_D[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','0','0'};

char note_Saturday_AS[20] = {'N','0','0','S','N','0','S','0','N','0','0','S','N','0','S','0'};  //10
char note_Saturday_BS[20] = {'N','0','0','S','N','0','S','0','N','0','0','S','N','0','S','0'};  //11
char note_Saturday_CS[20] = {'N','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};  //17
char note_Saturday_DS[20] = {'V','0','0','0','V','0','0','0','0','0','0','0','0','0','0','0'};  //29
char note_Saturday_ES[20] = {'N','0','0','S','N','0','S','0','U','0','0','R','0','0','0','0'};  //92
char note_Saturday_FS[20] = {'0','0','0','0','N','0','0','0','N','0','0','0','N','0','0','0'};  //93
char note_Saturday_GS[20] = {'T','0','0','S','T','0','S','0','0','0','0','0','0','0','0','0'};  //49
char note_Saturday_HS[20] = {'T','0','0','S','T','0','S','0','T','0','0','S','T','0','S','0'};  //46

//Balam Pichkari
char note_BalamPichkari_A[20] = {'N','S','T','S','T','0','0','0','0','0','0','0','0','0','K','0'};  //7
char note_BalamPichkari_B[20] = {'N','S','T','S','T','0','K','0','N','S','T','S','T','0','K','0'};  //11
char note_BalamPichkari_C[20] = {'N','S','T','S','T','0','K','0','K','0','0','0','0','0','0','0'};  //14
char note_BalamPichkari_D[20] = {'N','0','T','S','T','0','N','0','N','0','T','S','T','0','N','0'};  //15
char note_BalamPichkari_E[20] = {'N','0','T','S','T','0','N','0','K','0','0','K','0','0','K','0'};  //39
char note_BalamPichkari_F[20] = {'N','0','T','K','0','0','K','0','S','0','0','M','0','0','K','0'};  //68
char note_BalamPichkari_G[20] = {'N','0','T','S','T','0','N','0','N','S','T','S','K','0','0','0'};  //80
char note_BalamPichkari_H[20] = {'N','0','T','S','T','0','T','0','N','S','T','S','T','S','N','0'};  //96
char note_BalamPichkari_I[20] = {'K','0','0','S','0','0','K','0','N','0','T','S','T','0','N','0'};  //81

char Beat_Zingaat_A[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','0','0'};  //6
char Beat_Zingaat_B[20] = {'K','0','K','0','K','0','K','0','K','0','K','0','K','0','K','0'};  //12&13
char Beat_Zingaat_C[20] = {'N','0','P','0','N','0','P','0','N','0','P','0','N','0','P','0'};  //14
char Beat_Zingaat_D[20] = {'N','0','0','0','0','0','0','0','N','0','0','0','0','0','0','0'};  //32
char Beat_Zingaat_E[20] = {'N','0','N','0','N','0','N','0','N','0','0','0','0','0','0','0'};  //33
char Beat_Zingaat_F[20] = {'N','0','P','0','N','0','P','0','K','0','0','0','0','0','0','0'};  //51
char Beat_Zingaat_G[20] = {'N','0','T','0','N','0','T','0','N','0','T','0','N','0','T','0'};  //52
char Beat_Zingaat_H[20] = {'N','0','N','0','N','0','N','0','N','0','0','0','0','0','S','0'};  //57
char Beat_Zingaat_I[20] = {'N','0','0','0','0','0','0','0','K','0','0','0','0','0','0','0'};  //68 
char Beat_Zingaat_J[20] = {'S','S','H','H','M','M','F','F','N','0','0','0','0','0','0','0'};  //87
char Beat_Zingaat_K[20] = {'N','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};  //122

char Beat_LailaMeLaila_A[20] = {'V','0','W','0','V','0','0','0','V','W','0','0','V','W','0','0'};  //4
char Beat_LailaMeLaila_B[20] = {'V','0','W','0','V','0','W','0','V','0','W','0','V','0','W','0'};  //12                                                             
char Beat_LailaMeLaila_C[20] = {'K','0','T','S','N','0','P','0','N','0','T','S','N','0','P','0'};  //14
char Beat_LailaMeLaila_D[20] = {'N','0','T','S','N','0','P','0','N','0','T','S','N','0','P','0'};  //15& 20
char Beat_LailaMeLaila_E[20] = {'N','0','T','S','N','0','P','0','N','0','T','S','N','0','S','S'};  //21
char Beat_LailaMeLaila_F[20] = {'S','0','S','0','H','0','H','0','M','0','M','0','F','0','F','0'};  //45&70
char Beat_LailaMeLaila_G[20] = {'K','0','0','K','T','0','K','0','T','0','0','0','T','0','0','0'};  //62
char Beat_LailaMeLaila_H[20] = {'N','0','0','K','T','0','K','0','T','0','0','0','T','0','0','0'};  //63
char Beat_LailaMeLaila_I[20] = {'K','0','0','0','N','0','T','0','N','0','T','0','N','0','T','0'};  //71
char Beat_LailaMeLaila_J[20] = {'N','0','T','0','N','0','T','0','N','0','T','0','N','0','T','0'};  //72
char Beat_LailaMeLaila_K[20] = {'V','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};  //5

void setup() {
   // put your setup code here, to run once:
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Neck.attach(NeckPin);
  Hihat.attach(HihatPin);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Drummer_ready");
}

void Neckmovement(){
  if(Current_angle == Neck_up){
  Neck.write(Neck_down);
  Current_angle = Neck_down;
  }
else if (Current_angle == Neck_down){
  Neck.write(Neck_up);
  Current_angle = Neck_up;
  }
}

//=======================================================

void noteBadtmeezDil_B(){
      if(Beat_BadtmeezDil_B[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
        }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
      else if(Beat_BadtmeezDil_B[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
          }
}

void noteBadtmeezDil_D(){
        if(Beat_BadtmeezDil_D[i] == 'K'){
           Kick.write(Kick_down);
           Serial.println(i);
          }
        else if(Beat_BadtmeezDil_D[i] == 'S'){
           Snare.write(Snare_down);
           Serial.println(i);
          }
}

void noteBadtmeezDil_E(){
        if(Beat_BadtmeezDil_E[i] == 'K'){
           Kick.write(Kick_down);
           Serial.println(i);
            }
        
         else if(Beat_BadtmeezDil_E[i] == 'S'){
           Snare.write(Snare_down);
           Serial.println(i);
            }
}
void BeatBadtmeezDil_1(){
for(j = 2; j<=5; j++){ 
    for(i=0; i<=15; i++) {
        if(j == 5){
            if(Beat_BadtmeezDil_J[i] == 'K'){
                   Kick.write(Kick_down);
                   Serial.println(i);
                   }
            else if(Beat_BadtmeezDil_J[i] == 'S'){
                   Snare.write(Snare_down);
                   Serial.println(i);
                   }    
                }
        else{
            if(Beat_BadtmeezDil_A[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
            }
        }
        delay(delay_BadtmeezDil);
          Kick.write(Kick_up);
          Snare.write(Snare_up);
      }
      Neckmovement();
  }
}

//=====================================================
void BeatBadtmeezDil_2(){
  for(j=6; j<= 39; j++){
    for (i=0; i<=15; i++){
        if(j == 13 || j == 21 || j == 31){
            if(Beat_BadtmeezDil_C[i] == 'K'){
              Kick.write(Kick_down);
              Serial.println(i);
              }
        
            else if(Beat_BadtmeezDil_C[i] == 'S'){
              Snare.write(Snare_down);
              Serial.println(i);
            }
        }
        else if(j == 39){
              noteBadtmeezDil_E();
          }
        else {
          noteBadtmeezDil_B();
        }
      delay(delay_BadtmeezDil);
      Kick.write(Kick_up);
      Snare.write(Snare_up);
    }
    Neckmovement();
  }
}
//=======================================================
void BeatBadtmeezDil_4(){
  for(j=40; j<=47; j++){
        for(i=0; i<=15; i++){
            if(j == 43){
                if(Beat_BadtmeezDil_G[i] == 'K'){
                    Kick.write(Kick_down);
                    Serial.println(i);     
                    }
                else if(Beat_BadtmeezDil_G[i] == 'S'){
                   Snare.write(Snare_down);
                   Serial.println(i);
                    }
              }
            else{
              if(Beat_BadtmeezDil_F[i] == 'K'){
                Kick.write(Kick_down);
                Serial.println(i);
                }
              else if(Beat_BadtmeezDil_F[i] == 'S'){
                 Snare.write(Snare_down);
                 Serial.println(i);
                  }
            }
            delay(delay_BadtmeezDil);
            Kick.write(Kick_up);
            Snare.write(Snare_up);
      }
      Neckmovement();
  }
}
//=========================================================
void BeatBadtmeezDil_5(){
  for(j=48; j<=55; j++){
 
      for(i=0; i<=15; i++){
          if(j == 49 || j == 53){
                if(Beat_BadtmeezDil_H[i] == 'K'){
                    Kick.write(Kick_down);
                    Serial.println(i);
                    }
                else if(Beat_BadtmeezDil_H[i] == 'S'){
                    Snare.write(Snare_down);
                    Serial.println(i);
                    }    
            }
           else if(j == 51){
                if(Beat_BadtmeezDil_I[i] == 'K'){
                   Kick.write(Kick_down);
                   Serial.println(i);
                   }
                else if(Beat_BadtmeezDil_I[i] == 'S'){
                   Snare.write(Snare_down);
                   Serial.println(i);
                   }    
               }
           else {
                  if(Beat_BadtmeezDil_A[i] == 'K'){
                  Kick.write(Kick_down);
                  Serial.println(i);
                  }
            }
      delay(delay_BadtmeezDil);
      Kick.write(Kick_up);
      Snare.write(Snare_up); 
      }
      Neckmovement();
  }
}
//============================================================
void BeatBadtmeezDil_6(){
  for(j = 56; j<=79 ; j++){
      for(i=0; i<=15; i++) {
      //Serial.println(i);
          if(j == 61 || j == 71){
              noteBadtmeezDil_D();
          } 
          else if(j == 79){
              noteBadtmeezDil_E();
          }
          else{
              noteBadtmeezDil_B();
          }
        delay(delay_BadtmeezDil);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
      }
      Neckmovement();
  }
}
//=========================================================
void BeatBadtmeezDil_9(){
  for(i=0; i<=15; i++) {
      //Serial.println(i);
        if(Beat_BadtmeezDil_A[i] == 'K'){
         Kick.write(Kick_down);
         Serial.println(i);
        }
        delay(delay_BadtmeezDil);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
     }
  BeatBadtmeezDil_5();
  Neckmovement();
}
//=========================================================================
void BeatBadtmeezDil_10(){
  for(j = 89; j<=102 ; j++){
      for(i=0; i<=15; i++) {
      //Serial.println(i);
          if(j == 94){
              noteBadtmeezDil_D();
          }  
          else if(j == 102){
              noteBadtmeezDil_E();
          }
          else{
              noteBadtmeezDil_B();
          }
        delay(delay_BadtmeezDil);
        Kick.write(Kick_up);
        Snare.write(Snare_up);
      }
      Neckmovement();
  }
}
void BadtmeezDil_Track(){
  BeatBadtmeezDil_1();
  BeatBadtmeezDil_2();
  BeatBadtmeezDil_4();
  BeatBadtmeezDil_5();
  BeatBadtmeezDil_6();
  BeatBadtmeezDil_9();
  BeatBadtmeezDil_10();
}
//=====================================================================

void Chammak_Challo_Fill() {

  for (i = 0; i <=15; i++) {
      if(note_Chammak_Challo_A[i] == 'H'){
        Tom_H.write(Tom_H_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_A[i] == 'M'){
        Tom_M.write(Tom_M_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_A[i] == 'F'){
        Floor_Tom.write(Floor_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_A[i] == 'S'){
        Snare.write(Snare_down);
        //Serial.println(i);
      }
      delay(delay_ChammakChallo);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  } 
}

void Chammak_Challo_Groove1() {
for(i = 0; i <= 15 ;i++){ 
      if(note_Chammak_Challo_B[i] == 'H'){
        Tom_H.write(Tom_H_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'M'){
        Tom_M.write(Tom_M_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'F'){
        Floor_Tom.write(Floor_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'S'){
        Snare.write(Snare_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'K'){
        Kick.write(Kick_down);
        //Serial.println(i);
      }
      else if(note_Chammak_Challo_B[i] == 'L'){
        Kick.write(Kick_down);
        //Serial.println(i);
      }
      delay(delay_ChammakChallo);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  } 
}

void Chammak_Challo_Groove3(){
  for(j=0; j<3; j++){  
      Chammak_Challo_Groove1();
  }
}

void Chammak_Challo_Groove4(){
  for(j=0; j<4; j++){  
      Chammak_Challo_Groove1();
  }
  
}
void Chammak_Challo_Groove7(){
      Chammak_Challo_Groove3();
      Chammak_Challo_Groove4();
}

void Chammak_Challo_Blank(){
  for(i = 0; i<=15 ; i++){
      if(note_Chammak_Challo_C[i] == 'L'){
        Kick.write(Kick_down);
       // Serial.println(i);
      }
    delay(delay_ChammakChallo);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  }
}

void Chammak_Challo_Fill_Groove7(){
  Chammak_Challo_Fill();
  Chammak_Challo_Groove7();
}

void Chammak_Challo_Fill_Groove3(){ 
  Chammak_Challo_Fill();
  Chammak_Challo_Groove3();
}
void Chammak_Challo_Blank_Groove3(){
  Chammak_Challo_Blank();
  Chammak_Challo_Groove3();
}

void Chammak_Challo_Blank_Groove7(){
  Chammak_Challo_Blank();
  Chammak_Challo_Groove7();
}

void Chammak_Challo_Track(){
        delay(1000);
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Blank_Groove3();
        Chammak_Challo_Blank_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Blank_Groove3();
        Chammak_Challo_Blank_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Blank_Groove7();
        Chammak_Challo_Groove4();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill_Groove7();
        Chammak_Challo_Fill();
}
//====================================================
void DhoomMachalle_1() {
for(j = 0; j<=2 ; j++){
  for (i = 0; i <=15; i++) {
      if(note_Dhoom_A[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Dhoom_A[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_Dhoom);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  }
  Neckmovement(); 
  }
}

void DhoomMachalle_2() {
  for (i = 0; i <=15; i++) {
      if(note_Dhoom_B[i] == 'N'){
        Hihat.write(Hihat_down);
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(delay_Dhoom);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  }
  Neckmovement(); 
}

void DhoomMachalle_3() {
  for (i = 0; i <=15; i++) {
      if(note_Dhoom_C[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Dhoom_C[i] == 'P'){
        Hihat.write(Hihat_down);
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Dhoom_C[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      else if(note_Dhoom_C[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_Dhoom);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  }
  Neckmovement(); 
}

void DhoomMachalle_4() {
  for (i = 0; i <=15; i++) {
      if(note_Dhoom_D[i] == 'K'){
        Kick.write(Kick_down);       
        Serial.println(i);
      }
      delay(delay_Dhoom);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  }
  Neckmovement(); 
}

void DhoomMachalle_5(){
  for (j = 10; j <= 93; j++){
     if(j == 21 || j == 77 || j ==57){
      DhoomMachalle_4();
     }
     else if(j == 22 || j == 78 || j == 58){
      Serial.println("Silent loop");
      for (k = 1; k <= 16; k++){
      delay(delay_Dhoom);
      }
     }
     else{
       DhoomMachalle_3();
     }
  }
}
//==========================================
void BeatSaturday_10(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_AS[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Saturday_AS[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Saturday_AS[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_11(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_BS[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Saturday_BS[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_17(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_CS[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_29(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_DS[i] == 'V'){
        Kick.write(Kick_down);
        Snare.write(Snare_down);
        Floor_Tom.write(Floor_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_92(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_ES[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_Saturday_ES[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Saturday_ES[i] == 'U'){
        Kick.write(Kick_down);
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Saturday_ES[i] == 'R'){
        Kick.write(Kick_down);
        Tom_M.write(Tom_M_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_46(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_HS[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Saturday_HS[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatSaturday_49(){
    for (i = 0; i <=15; i++) {
      if(note_Saturday_GS[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_Saturday_GS[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      delay(delay_Saturday);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void Saturday_Track(){
  for(j = 10; j<=92 ; j++){
      if(j == 10 || j == 18 || j == 30 || j == 58 || j == 78){
          BeatSaturday_10();
      }
      else if(j == 17 || j==26 || j==27 || j==28 || j == 45 || j==54 || j==55 || j==56 || j==74 || j==75 || j==76 ){
          BeatSaturday_17();
      }
      else if(j == 29 || j == 57 || j == 77){
          BeatSaturday_29();
      }
      else if(j == 49 || j == 69){
        BeatSaturday_49();
      }
      else if(j == 92){
          BeatSaturday_92();
      }
      else if(j == 46 || j==47 || j==48 || j==65 || j==66 || j==67 || j==68 || j==85){
          BeatSaturday_46();
      }
      else{
          BeatSaturday_11();    
      }
  }
}
//==========================================================================Balam Pichkari

void BeatBalamPichkari_7() {
for(j = 7; j<=10 ; j++){
  for (i = 0; i <=15; i++) {
      if(note_BalamPichkari_A[i] == 'N'){
        Kick.write(Kick_down);
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_BalamPichkari_A[i] == 'T'){
        Hihat.write(Hihat_down);
        Serial.println(i);
      }
      else if(note_BalamPichkari_A[i] == 'S'){
        Snare.write(Snare_down);
        Serial.println(i);
      }
      else if(note_BalamPichkari_A[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
      }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}
}

//11
void BeatBalamPichkari_11() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_B[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_B[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_B[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_B[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

//14
void BeatBalamPichkari_14() {
  for (i = 0; i <=15; i++){
          if(note_BalamPichkari_C[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_C[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_C[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_C[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
    Tom_H.write(Tom_H_up);
    Tom_M.write(Tom_M_up);
    Snare.write(Snare_up);
    Floor_Tom.write(Floor_up);
    Kick.write(Kick_up);
    Hihat.write(Hihat_up);
    }
}

//15
void BeatBalamPichkari_15() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_D[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_D[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_D[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_D[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

//39
void BeatBalamPichkari_39() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_E[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_E[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_E[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_E[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatBalamPichkari_68() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_F[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_F[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_F[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_F[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatBalamPichkari_80() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_G[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_G[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_G[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_G[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatBalamPichkari_81() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_I[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_I[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_I[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_I[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}
void BeatBalamPichkari_96() {
  for (i = 0; i <=15; i++) {             
          if(note_BalamPichkari_H[i] == 'N'){
            Kick.write(Kick_down);
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_H[i] == 'T'){
            Hihat.write(Hihat_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_H[i] == 'S'){
            Snare.write(Snare_down);
            Serial.println(i);
          }
          else if(note_BalamPichkari_H[i] == 'K'){
            Kick.write(Kick_down);
            Serial.println(i);
          }
      delay(delay_BalamPichkari);
  Tom_H.write(Tom_H_up);
  Tom_M.write(Tom_M_up);
  Snare.write(Snare_up);
  Floor_Tom.write(Floor_up);
  Kick.write(Kick_up);
  Hihat.write(Hihat_up);
  } 
}

void BeatBalamPichkari_Track(){
  for(k = 11; k<=96; k++){
    if(k == 14 || k == 51){
      BeatBalamPichkari_14();
    }
    else if(k == 39){
      BeatBalamPichkari_39();
    }
    else if(k == 68){
      BeatBalamPichkari_68();
    }
    else if(k == 80){
      BeatBalamPichkari_80();
    }
    else if(k == 81){
      BeatBalamPichkari_81();
    }
    else if(k == 96){
      BeatBalamPichkari_96();
    }
    else if(k==11 || k==12 || k==13){
      BeatBalamPichkari_11();
    }
    else  {
      BeatBalamPichkari_15();
    }
  }
}
//====================================

void noteLailaMeLaila_A(){
  for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_A[i] == 'V'){
        Kick.write(Kick_down);
        Tom_M.write(Tom_M_down);
        Floor_Tom.write(Floor_down);
        Serial.println(i);
        }
      else if(Beat_LailaMeLaila_A[i] == 'W'){
         Tom_M.write(Tom_M_down);
         Floor_Tom.write(Floor_down);
         Serial.println(i);
         }
         delay(delay_LailaMeLaila);     
      Tom_M.write(Tom_M_up);
      Floor_Tom.write(Floor_up);
      Kick.write(Kick_up);
  }
  Neckmovement();
}
void noteLailaMeLaila_B(){
 for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_B[i] == 'V'){
        Kick.write(Kick_down);
        Tom_M.write(Tom_M_down);
        Floor_Tom.write(Floor_down);
        Serial.println(i);
        }
      else if(Beat_LailaMeLaila_B[i] == 'W'){
         Tom_M.write(Tom_M_down);
         Floor_Tom.write(Floor_down);
         Serial.println(i);
         }
         delay(delay_LailaMeLaila);     
      Tom_M.write(Tom_M_up);
      Floor_Tom.write(Floor_up);
      Kick.write(Kick_up);
  }
  Neckmovement();
}
void noteLailaMeLaila_C(){
 for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_C[i] == 'K'){
        Kick.write(Kick_down);
        Serial.println(i);
        }
      else if(Beat_LailaMeLaila_C[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_C[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_C[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_C[i] == 'P'){
         Hihat.write(Hihat_down);
         Snare.write(Snare_down);
         Serial.println(i);
         }  
         delay(delay_LailaMeLaila);
      Snare.write(Snare_up);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_D(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_D[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_D[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_D[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_D[i] == 'P'){
         Hihat.write(Hihat_down);
         Snare.write(Snare_down);
         Serial.println(i);
         }  
         delay(delay_LailaMeLaila);     
      Snare.write(Snare_up);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_E(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_E[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_E[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_E[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_E[i] == 'P'){
         Hihat.write(Hihat_down);
         Snare.write(Snare_down);
         Serial.println(i);
         }  
         delay(delay_LailaMeLaila);
      Snare.write(Snare_up);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_F(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_F[i] == 'H'){
         Tom_H.write(Tom_H_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_F[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_F[i] == 'M'){
         Tom_M.write(Tom_M_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_F[i] == 'F'){
         Floor_Tom.write(Floor_down);
         Serial.println(i);
         } 
         delay(delay_LailaMeLaila);
      Tom_H.write(Tom_H_up);
      Tom_M.write(Tom_M_up);
      Snare.write(Snare_up);
      Floor_Tom.write(Floor_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_G(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_G[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_G[i] == 'K'){
         Kick.write(Kick_down);
         Serial.println(i);
         }
         delay(delay_LailaMeLaila);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_H(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_H[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_H[i] == 'K'){
         Kick.write(Kick_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_H[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      delay(delay_LailaMeLaila);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_I(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_I[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_I[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
         delay(delay_LailaMeLaila);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}
void noteLailaMeLaila_J(){
for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_J[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_LailaMeLaila_J[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      delay(delay_LailaMeLaila);
    Kick.write(Kick_up);
    Hihat.write(Hihat_up);
  }
  Neckmovement();
}

void noteLailaMeLaila_K(){
  for(i = 0; i <= 15 ;i++){
      if(Beat_LailaMeLaila_K[i] == 'V'){
        Kick.write(Kick_down);
        Tom_M.write(Tom_M_down);
        Floor_Tom.write(Floor_down);
        Serial.println(i);
        }
        delay(delay_LailaMeLaila);
      Tom_M.write(Tom_M_up);
      Floor_Tom.write(Floor_up);
      Kick.write(Kick_up);
  }
  Neckmovement();
}
void LailaMeLaila_Track(){
  for(j = 4; j<=112; j++){
    if(j==4 || j==7 || j==10){
      noteLailaMeLaila_A();
    }
    else if(j==5 || j==8 || j==11 || j==13){
      noteLailaMeLaila_K();
    }
    else if(j==12){
      noteLailaMeLaila_B();
    }
    else if(j==14 || j == 22 || j==30 || j == 38 || j==46 || j==81 || j==89 || j==105 || j == 111){
      noteLailaMeLaila_C();
    }
    else if(j == 21 || j==29 || j==37 || j==47 || j==80 || j==90){
      noteLailaMeLaila_E();
    }
    else if(j==45 || j==61 || j==79 || j==88 || j==112 || j==70){
      noteLailaMeLaila_F();
    }
    else if(j==62){
      noteLailaMeLaila_G();
    }
    else if(j>62 && j<70){
      noteLailaMeLaila_H();
    }
    else if(j==71){
      noteLailaMeLaila_I();
    }
    else if(j>71 && j<79){
      noteLailaMeLaila_J();
    }
    else if(j==6 || j==9){
      delay(1724);
    }
    else{
      noteLailaMeLaila_D();
    }
  }
}
//=========================================


void noteZingaat_A(){
  for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_A[i] == 'K'){
          Kick.write(Kick_down);
          Serial.println(i);
          }
         delay(delay_Zingaat);           
      Kick.write(Kick_up);
  }
  Neckmovement();
}

void noteZingaat_B(){
 for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_B[i] == 'K'){
          Kick.write(Kick_down);
          Serial.println(i);
          }
         delay(delay_Zingaat);           
      Kick.write(Kick_up);
  }
  Neckmovement();
}

void noteZingaat_C(){
 for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_C[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_C[i] == 'P'){
         Hihat.write(Hihat_down);
         Snare.write(Snare_down);
         Serial.println(i);
         }  
         delay(delay_Zingaat);
      Snare.write(Snare_up);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}

void noteZingaat_D(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_D[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
         delay(delay_Zingaat);     
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}

void noteZingaat_E(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_E[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
         delay(delay_Zingaat);     
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}

void noteZingaat_F(){
for(i = 0; i <= 15 ;i++){
       if(Beat_Zingaat_F[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_F[i] == 'K'){
         Kick.write(Kick_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_F[i] == 'P'){
         Hihat.write(Hihat_down);
         Snare.write(Snare_down);
         Serial.println(i);
         }
         delay(delay_Zingaat);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
      Snare.write(Snare_up);
    }
    Neckmovement();
}

void noteZingaat_G(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_G[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_G[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
         delay(delay_Zingaat);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}

void noteZingaat_H(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_H[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_H[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      delay(delay_Zingaat);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
      Snare.write(Snare_up);
    }
    Neckmovement();
}

void noteZingaat_I(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_I[i] == 'K'){
          Kick.write(Kick_down);
          Serial.println(i);
          }
      else if(Beat_Zingaat_I[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
         delay(delay_Zingaat);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}

void noteZingaat_J(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_J[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_J[i] == 'H'){
         Tom_H.write(Tom_H_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_J[i] == 'M'){
         Tom_M.write(Tom_M_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_J[i] == 'F'){
         Floor_Tom.write(Floor_down);
         Serial.println(i);
         } 
      else if(Beat_Zingaat_J[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      delay(delay_Zingaat);
    Kick.write(Kick_up);
    Hihat.write(Hihat_up);
    Tom_H.write(Tom_H_up);
    Tom_M.write(Tom_M_up);
    Snare.write(Snare_up);
    Floor_Tom.write(Floor_up);
  }
  Neckmovement();
}

void noteZingaat_K(){
  for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_K[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
        delay(delay_Zingaat);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
  }
  Neckmovement();
}
 
void Zingaat_Track(){
  for(j = 6; j<=122; j++){
    if(j>=6 && j<=9){
      noteZingaat_A();
    }
    else if(j>=10 && j<=13){
      noteZingaat_B();
    }
    else if(j==32){
      noteZingaat_D();
    }
    else if(j==33 || j == 63 || j==69 || j == 99 || j==105){
      noteZingaat_E();
    }
    else if(j==51){
      noteZingaat_F();
    }
    else if(j>=52 && j<=56){
      noteZingaat_G();
    }
    else if(j>=88 && j<=92){
      noteZingaat_G();
    }
    else if(j==57 || j==93){
      noteZingaat_H();
    }
    else if(j == 68 || j==104){
      noteZingaat_I();
    }
    else if(j==87){
      noteZingaat_J();
    }
    else if(j==122){
      noteZingaat_K();
    }
    else{
      noteZingaat_C();
    }
  }
}

//=================================================

void loop()  {
 
 if (Serial.available())
  { 
    int state = Serial.parseInt();
    Serial.println(state);
    Serial.println("Drummer_ready");
      if (state == 2)
      { 
        delay(1500);
        BadtmeezDil_Track();
      }
      else if(state == 3){
        //Serial.print("Drumm");
        Chammak_Challo_Track();    
      }
      else if (state == 4)
      {
        delay(11500);
        BeatBalamPichkari_7();
        BeatBalamPichkari_Track();
      }
      else if(state == 5){
        delay(15750);
        Saturday_Track();  
        Serial.println("The end");
      }
      else if(state == 6)
      { 
        Serial.print("Drumm");
        delay(4500);
        LailaMeLaila_Track();
      }
      else if (state == 7)
      { 
        Serial.print("Drumm");
        delay(9500);
        Zingaat_Track();
      }
      else if (state == 8)
      {
        delay(10750);
        DhoomMachalle_1();
        DhoomMachalle_2();
        DhoomMachalle_5();
        Serial.println("THE END");
      }
  }
}
