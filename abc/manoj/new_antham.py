
from __future__ import division
import time
import Adafruit_PCA9685
import pygame
pygame.mixer.init()
pwm = Adafruit_PCA9685.PCA9685()
servo_min = 150  # Min pulse length out of 4096
servo_max = 600  # Max pulse length out of 4096

# Helper function to make setting a servo pulse width simpler.
def set_servo_pulse(channel, pulse):
    pulse_length = 1000000    # 1,000,000 us per second
    pulse_length //= 60       # 60 Hz
    print('{0}us per period'.format(pulse_length))
    pulse_length //= 4096     # 12 bits of resolution
    print('{0}us per bit'.format(pulse_length))
    pulse *= 1000
    pulse //= pulse_length
    pwm.set_pwm(channel, 0, pulse)

# Set frequency to 60hz, good for servos.
pwm.set_pwm_freq(60)
pygame.mixer.music.load("/home/pi/manoj/medifiles/national_anthem.mid")
pygame.mixer.music.play()                                      
def beat1():
    for i in range(0,12):
        pwm.set_pwm(15, 0, 220)
        time.sleep(1)
        
        pwm.set_pwm(15, 0, 325)
        pwm.set_pwm(0, 0, 220)
        time.sleep(1)
        
        pwm.set_pwm(0, 0, 325)
        time.sleep(1)
       # time.sleep(1)
        pwm.set_pwm(7, 0, 325)
        time.sleep(1)
        #time.sleep(1)
        pwm.set_pwm(7, 0, 220)
        time.sleep(1)
        #time.sleep(1)
        pwm.set_pwm(8, 0, 325)
        time.sleep(1)
        pwm.set_pwm(8, 0, 220)
        pwm.set_pwm(15, 0, 220)
        time.sleep(1)
        pwm.set_pwm(7, 0, 325)
        time.sleep(1)
        pwm.set_pwm(15, 0, 325)
        pwm.set_pwm(7, 0, 220)

        
def beat2():
    for i in range(0,4):
            
        pwm.set_pwm(15, 0, 220)
        time.sleep(1)
       # time.sleep(1)
        pwm.set_pwm(15, 0, 325)
        pwm.set_pwm(0, 0, 220)
        time.sleep(1)
       # time.sleep(1)
        pwm.set_pwm(0, 0, 325)
        time.sleep(1)
        #time.sleep(1)
        pwm.set_pwm(7, 0, 325)
        time.sleep(1)
        #time.sleep(1)
        pwm.set_pwm(7, 0, 220)
        time.sleep(1)
       # time.sleep(1)
        pwm.set_pwm(8, 0, 325)
        time.sleep(1)
        pwm.set_pwm(8, 0, 220)
        pwm.set_pwm(15, 0, 220)
        time.sleep(1)
        pwm.set_pwm(7, 0, 325)
        time.sleep(1)
        pwm.set_pwm(15, 0, 325)
        pwm.set_pwm(7, 0, 220)
def beat3():
    for i in range(0,3):
        pwm.set_pwm(8, 0, 325)
        time.sleep(1)
        pwm.set_pwm(8, 0, 220)
        pwm.set_pwm(15, 0, 220)
        time.sleep(1)
        pwm.set_pwm(15, 0, 325)
        pwm.set_pwm(0, 0, 220)
        time.sleep(1)
        pwm.set_pwm(0, 0, 325)
        pwm.set_pwm(7, 0, 325)
        time.sleep(1)
        pwm.set_pwm(7, 0, 220)

if __name__=='__main__':
    beat1()
    time.sleep(12)
    beat2()
    time.sleep(6)
    beat3()
    time.sleep(3)

         ## Hihat.stop()
         ## Bass.stop()
 #   GPIO.cleanup()
