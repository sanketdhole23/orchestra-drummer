#include <Servo.h>
// Song Name :Zingaat, Movie Name: , 
int i, j, k;
int delay_Zingaat = 125;
int TomHPin = 6;
int TomMPin = 7;
int FloorPin = 8;
int KickPin = 9;
int NeckPin = 10;
int HihatPin = 11;
int SnarePin = 12;
int Snare_up = 95;
int Floor_up = 70;
int Tom_H_up = 80;
int Tom_M_up = 85;
int Neck_up = 90;
int Hihat_up = 90;
int Kick_up = 90;                                             
int Snare_down = 105;
int Floor_down = 60;
int Tom_H_down = 70;
int Tom_M_down = 95;
int Neck_down = 140;
int Hihat_down = 80;
int Kick_down = 105;
int Current_angle = 90;
Servo Tom_H;    //H
Servo Tom_M;    //M
Servo Floor_Tom;//F
Servo Snare;    //S
Servo Neck;    //C
Servo Hihat;     //T
Servo Kick;     //K
                // C+K = L   T+K = N   T+S = P  M+T+S+K = Q   M+K = R   S+K = U  M+F+K = V  M+F = W   T+S+K = X
char Beat_Zingaat_A[20] = {'K','0','0','0','K','0','0','0','K','0','0','0','K','0','0','0'};  //6
char Beat_Zingaat_B[20] = {'K','0','K','0','K','0','K','0','K','0','K','0','K','0','K','0'};  //12&13
char Beat_Zingaat_C[20] = {'N','0','P','0','N','0','P','0','N','0','P','0','N','0','P','0'};  //14
char Beat_Zingaat_D[20] = {'N','0','0','0','0','0','0','0','N','0','0','0','0','0','0','0'};  //32
char Beat_Zingaat_E[20] = {'N','0','N','0','N','0','N','0','N','0','0','0','0','0','0','0'};  //33
char Beat_Zingaat_F[20] = {'N','0','P','0','N','0','P','0','K','0','0','0','0','0','0','0'};  //51
char Beat_Zingaat_G[20] = {'N','0','T','0','N','0','T','0','N','0','T','0','N','0','T','0'};  //52
char Beat_Zingaat_H[20] = {'N','0','N','0','N','0','N','0','N','0','0','0','0','0','S','0'};  //57
char Beat_Zingaat_I[20] = {'N','0','0','0','0','0','0','0','K','0','0','0','0','0','0','0'};  //68 
char Beat_Zingaat_J[20] = {'S','S','H','H','M','M','F','F','N','0','0','0','0','0','0','0'};  //87
char Beat_Zingaat_K[20] = {'N','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};  //122
void setup() {
   // put your setup code here, to run once:
  Tom_H.attach(TomHPin);
  Tom_M.attach(TomMPin);
  Floor_Tom.attach(FloorPin);
  Snare.attach(SnarePin);
  Kick.attach(KickPin);
  Neck.attach(NeckPin);
  Hihat.attach(HihatPin);
  
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Drummer_ready");
 }

//===========================================================

void Neckmovement(){
  if(Current_angle == Neck_up){
  Neck.write(Neck_down);
  Current_angle = Neck_down;
  }
  else if (Current_angle == Neck_down){
  Neck.write(Neck_up);
  Current_angle = Neck_up;
  }
}

void noteZingaat_A(){
  for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_A[i] == 'K'){
          Kick.write(Kick_down);
          Serial.println(i);
          }
         delay(delay_Zingaat);           
      Kick.write(Kick_up);
  }
  Neckmovement();
}

void noteZingaat_B(){
 for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_B[i] == 'K'){
          Kick.write(Kick_down);
          Serial.println(i);
          }
         delay(delay_Zingaat);           
      Kick.write(Kick_up);
  }
  Neckmovement();
}

void noteZingaat_C(){
 for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_C[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_C[i] == 'P'){
         Hihat.write(Hihat_down);
         Snare.write(Snare_down);
         Serial.println(i);
         }  
         delay(delay_Zingaat);
      Snare.write(Snare_up);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}

void noteZingaat_D(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_D[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
         delay(delay_Zingaat);     
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}

void noteZingaat_E(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_E[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
         delay(delay_Zingaat);     
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}

void noteZingaat_F(){
for(i = 0; i <= 15 ;i++){
       if(Beat_Zingaat_F[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_F[i] == 'K'){
         Kick.write(Kick_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_F[i] == 'P'){
         Hihat.write(Hihat_down);
         Snare.write(Snare_down);
         Serial.println(i);
         }
         delay(delay_Zingaat);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
      Snare.write(Snare_up);
    }
    Neckmovement();
}

void noteZingaat_G(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_G[i] == 'T'){
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_G[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
         delay(delay_Zingaat);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}

void noteZingaat_H(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_H[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_H[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      delay(delay_Zingaat);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
      Snare.write(Snare_up);
    }
    Neckmovement();
}

void noteZingaat_I(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_I[i] == 'K'){
          Kick.write(Kick_down);
          Serial.println(i);
          }
      else if(Beat_Zingaat_I[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
         delay(delay_Zingaat);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
    }
    Neckmovement();
}

void noteZingaat_J(){
for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_J[i] == 'S'){
         Snare.write(Snare_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_J[i] == 'H'){
         Tom_H.write(Tom_H_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_J[i] == 'M'){
         Tom_M.write(Tom_M_down);
         Serial.println(i);
         }
      else if(Beat_Zingaat_J[i] == 'F'){
         Floor_Tom.write(Floor_down);
         Serial.println(i);
         } 
      else if(Beat_Zingaat_J[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
      delay(delay_Zingaat);
    Kick.write(Kick_up);
    Hihat.write(Hihat_up);
    Tom_H.write(Tom_H_up);
    Tom_M.write(Tom_M_up);
    Snare.write(Snare_up);
    Floor_Tom.write(Floor_up);
  }
  Neckmovement();
}

void noteZingaat_K(){
  for(i = 0; i <= 15 ;i++){
      if(Beat_Zingaat_K[i] == 'N'){
         Kick.write(Kick_down);
         Hihat.write(Hihat_down);
         Serial.println(i);
         }
        delay(delay_Zingaat);
      Kick.write(Kick_up);
      Hihat.write(Hihat_up);
  }
  Neckmovement();
}
 
void Zingaat_Track(){
  for(j = 6; j<=122; j++){
    if(j>=6 && j<=9){
      noteZingaat_A();
    }
    else if(j>=10 && j<=13){
      noteZingaat_B();
    }
    else if(j==32){
      noteZingaat_D();
    }
    else if(j==33 || j == 63 || j==69 || j == 99 || j==105){
      noteZingaat_E();
    }
    else if(j==51){
      noteZingaat_F();
    }
    else if(j>=52 && j<=56 && j>=88 && j<=92){
      noteZingaat_G();
    }
    else if(j==57 && j==93){
      noteZingaat_H();
    }
    else if(j == 68 && j==104){
      noteZingaat_I();
    }
    else if(j==87){
      noteZingaat_J();
    }
    else if(j==122){
      noteZingaat_K();
    }
    else{
      noteZingaat_C();
    }
  }
}

void loop(){
  if (Serial.available())
  {
    int state = Serial.parseInt();   
      if (state == 7)
      { 
        Serial.print("Drumm");
        delay(9000);
        Zingaat_Track();
      }
  }
}
