#include <Servo.h>
// Program for National Anthem
int i;
int servoPin1 = 8;
int servoPin2 = 9;
int servoPin3 = 10;
int servoPin4 = 11;
int servopin5 = 12;
Servo Tom_L;
Servo Tom_R;
Servo Floor_Tom;
Servo Snare;
Servo neck;

char note_A[20] = {'L','R','F','0','S','0','L','F'};
void setup()

{
  Tom_L.attach(servoPin1);
  Tom_R.attach(servoPin2);
  Floor_Tom.attach(servoPin3);
  Snare.attach(servoPin4);
  neck.attach(servopin5);
  pinMode(13, OUTPUT);

  Serial.begin(9600);

  while (!Serial);

  //Serial.println("Input 1 to Turn LED on and 2 to off");

}

void beat1() {

  for (i = 0; i < 12; i++) {

    Tom_L.write(85);
    neck.write(90);
    delay(250);                     //0.25
    Tom_L.write(100);
    Tom_R.write(65);
    neck.write(120);
    delay(250);                     //0.5
    Tom_R.write(80);
    Floor_Tom.write(90);
    neck.write(90);
    delay(250);                     //0.75
    Floor_Tom.write(75);
    neck.write(120);
    delay(250);                     //1.0
    Snare.write(90);
    neck.write(90);
    delay(250);                       //1.25
    Snare.write(75);
    neck.write(120);
    delay(250);                       //1.5
    Tom_L.write(85);
    neck.write(90);
    delay(250);                     //1.75
    Tom_L.write(100);
    Floor_Tom.write(90);
    neck.write(120);
    delay(250);                   //2.0
    Floor_Tom.write(75);
    neck.write(90);

  }

}
void beat2() {
  for (i = 0; i < 4; i++) {


    Tom_L.write(85);
    neck.write(90);
    delay(250);                     //0.25
    Tom_L.write(100);
    Tom_R.write(65);
    neck.write(120);
    delay(250);                     //0.5
    Tom_R.write(80);
    Floor_Tom.write(90);
    neck.write(90);
    delay(250);                     //0.75
    Floor_Tom.write(75);
    neck.write(120);
    delay(250);                     //1.0
    Snare.write(90);
    neck.write(90);
    delay(250);                       //1.25
    Snare.write(75);
    neck.write(120);
    delay(250);                       //1.5
    Tom_L.write(85);
    neck.write(90);
    delay(250);                     //1.75
    Tom_L.write(100);
    Floor_Tom.write(90);
    neck.write(120);
    delay(250);                   //2.0
    Floor_Tom.write(75);
    neck.write(90);
  }
}
void beat3() {


  
  Snare.write(90);
  neck.write(90);
  delay(125);
  Snare.write(85);
  delay(125);
  Snare.write(95);
  delay(250);
  Snare.write(75);
  Tom_L.write(85);
  delay(125);
  Tom_L.write(100);
  delay(125);
  Tom_L.write(85);
  delay(250);                   //1
  Tom_L.write(100);
  Tom_R.write(60);
  delay(125);
  Tom_R.write(70);
  delay(125);
  Tom_R.write(60);
  delay(250);
  Tom_R.write(80);
  Floor_Tom.write(90);
  delay(125);
  Floor_Tom.write(80);
  delay(125);
  Floor_Tom.write(90);
  delay(250);
  Floor_Tom.write(75);                                      
  
}


void loop() {


/*
  while (Serial.available())

  {
    int state = Serial.parseInt();
    Serial.println(state);
    Serial.println("qwerty");
    Serial.flush();

    if (state == 1)
    {
      Serial.println("stage 1");
      digitalWrite(13, HIGH);
      beat1();
      delay(12000);
      beat2();
      delay(6000);
      beat3();
      delay(3000);
      //Serial.println("Command completed LED turned ON");
    }

    if (state == 2)

    {
      Serial.print("abc");
      digitalWrite(13, LOW);

      // Tom_L.write(100);
      //    Tom_R.write(80);
      Tom_L.detach();
      Tom_R.detach();
      Floor_Tom.detach();
      Snare.detach();
      neck.detach();
      //Serial.println("Command completed LED turned OFF");
    }
  }

*/

}
